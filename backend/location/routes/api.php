<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LocationController;
use App\Http\Controllers\HotelController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\TravelController;
use App\Http\Controllers\DirectionController;
/*use Helper;*/
use App\Http\Controllers\SocialiteController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/location', [LocationController::class, 'getCoordinateByString']);
Route::post('/location/pois', [LocationController::class, 'getPois']);
Route::post('/location/hotels', [HotelController::class, 'getHotelsByLocation']);
Route::post('/location/hotelById', [HotelController::class, 'getHotelById']);
Route::post('/location/direction', [DirectionController::class, 'getDirection']);
//Route::post('/test', [Helper::class, 'getCacheToken']);

Route::post('/login', [AuthController::class, 'login']);
Route::get('/logout', [AuthController::class, 'logout'])->middleware('jwt.verify');
//Route::get('/current-user', [AuthController::class, 'currentUser'])->middleware('jwt.verify');
//Route::post('/forgot-password', [AuthController::class, 'forgotPassword']);
//Route::post('/reset-password', [AuthController::class, 'submitResetPasswordForm']);

Route::get('/users/{id}', [UserController::class, 'getUser'])->middleware('jwt.verify');
Route::post('/users', [UserController::class, 'create']);
Route::patch('/users/{id}', [UserController::class, 'update'])->middleware('jwt.verify');
Route::delete('/users/{id}', [UserController::class, 'delete'])->middleware('jwt.verify');


Route::post('/travel/{userId}', [TravelController::class, 'createTravelPlan'])->middleware('jwt.verify');
Route::patch('/travel/{id}', [TravelController::class, 'updateTravelPlan'])->middleware('jwt.verify');
Route::delete('/travel/{id}', [TravelController::class, 'deleteTravelPlan'])->middleware('jwt.verify');
Route::get('/travel/{id}', [TravelController::class, 'getTravelPlan'])->middleware('jwt.verify');
Route::get('/user/{id}/travels', [TravelController::class, 'getTravelsByUserId'])->middleware('jwt.verify');

Route::get("redirect", [SocialiteController::class, 'redirect']);
Route::get("/google/login", [SocialiteController::class, 'callback']);
