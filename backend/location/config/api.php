<?php

return [
    'tests' => [
        'email' => env('API_USER_EMAIL'),
        'password' => env('API_USER_PASSWORD'),
        'username' => env('API_USER_USERNAME')
    ]
];
