<?php

namespace Tests\Feature;

use App\Models\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;
use Tymon\JWTAuth\Facades\JWTAuth;

class UserTest extends TestCase
{
    use DatabaseMigrations;
    protected const id_user = 0;
    protected const URL = '/api/users';
    protected const newUser = [
        "username" => "usertest",
        "email" => "usertest@test.fr",
        "password" => "password",
        "password_confirmation" => "password",
    ];
    protected const wrongNewUser = [
        "username" => "test",
        "email" => "test",
        "password" => "password",
    ];
    protected const updateUser = [
        "username" => "updatedTest",
        "email" => "new@test.test",
        "password_confirmation" => "newpassword",
        "old_password" => "password",
        "password" => "newpassword",
    ];
    protected const updateWrongUser = [
        "username" => 1
    ];

    protected function setUp(): void
    {
        parent::setUp();
        User::factory()->create([
            'id' => 0,
            'username' => config('api.tests.username'),
            'email' => config('api.tests.email'),
            'password' => Hash::make(config('api.tests.password')),
        ]);
    }
    public function test_OkGetUserTest()
    {
        DB::beginTransaction();
        $id = self::id_user;
        $email = config('api.tests.email');
        $password = config('api.tests.password');
        $username = config('api.tests.username');

        $credentials = ['email' => $email, 'password' => $password];
        $jwt = auth('api')->attempt($credentials);


        $response = $this->json('GET', self::URL . "/" . $id, self::newUser, ['Accept' => 'application/json', 'HTTP_Authorization' => 'Bearer ' . $jwt]);
        $response->assertJsonStructure([
            'data',
            'message',
            'status'
        ])
            ->assertJson([
                'data' => [
                    "username" => $username,
                    "email" => $email,
                    "id" => $id
                ],
                'message' => 'Get user successful',
                'status' => 1
            ])
            ->assertStatus(200);
        DB::rollBack();
    }

    public function test_KoGetUserTest()
    {
        DB::beginTransaction();
        $id = "fake";
        $email = config('api.tests.email');
        $password = config('api.tests.password');
        $username = config('api.tests.username');

        $credentials = ['email' => $email, 'password' => $password];
        $jwt = auth('api')->attempt($credentials);


        $response = $this->json('GET', self::URL . "/" . $id, self::newUser, ['Accept' => 'application/json', 'HTTP_Authorization' => 'Bearer ' . $jwt]);
        $response->assertJsonStructure([
            'data',
            'message',
            'status'
        ])
            ->assertJson([
                'data' => null,
                'message' => 'Wrong id given',
                'status' => 0
            ])
            ->assertStatus(422);
        DB::rollBack();
    }

    public function test_OkCreateUserTest()
    {
        DB::beginTransaction();

        $response = $this->json('POST', self::URL, self::newUser, ['Accept' => 'application/json']);
        $response->assertJsonStructure([
            'data',
            'message',
            'status'
        ])
            ->assertStatus(200);
        DB::rollBack();
    }

    public function test_KoCreateUserTest()
    {
        DB::beginTransaction();

        $response = $this->json('POST', self::URL, self::wrongNewUser, ['Accept' => 'application/json']);
        $response->assertJsonStructure([
            'data',
            'message',
            'status'
        ])
            ->assertJson([
                'data' => null,
                'message' => "{\"email\":[\"The email field must be a valid email address.\"],\"password\":[\"The password field confirmation does not match.\"]}",
                'status' => 0
            ])
            ->assertStatus(422);
        DB::rollBack();
    }

    public function test_OkUpdateUserTest()
    {
        DB::beginTransaction();
        $id = self::id_user;
        $email = config('api.tests.email');
        $password = config('api.tests.password');

        $credentials = ['email' => $email, 'password' => $password];
        $jwt = auth('api')->attempt($credentials);

        $response = $this->json('PATCH', self::URL. '/' . $id, self::updateUser, ['Accept' => 'application/json', 'HTTP_Authorization' => 'Bearer ' . $jwt]);
        $response->assertStatus(200)
            ->assertJson([
                'data' => [
                    "username" => self::updateUser["username"],
                    "email" => self::updateUser["email"],
                    "id" => $id
                ],
                'status' => 1
            ]);
        DB::rollBack();
    }

    public function test_KoUpdateUserTest()
    {
        DB::beginTransaction();
        $id = self::id_user;
        $email = config('api.tests.email');
        $password = config('api.tests.password');

        $credentials = ['email' => $email, 'password' => $password];
        $jwt = auth('api')->attempt($credentials);

        $response = $this->json('PATCH', self::URL. '/' . $id, self::updateWrongUser, ['Accept' => 'application/json', 'HTTP_Authorization' => 'Bearer ' . $jwt]);
        $response->assertStatus(422)
            ->assertJsonStructure([
                'data',
                'message',
                'status'
            ])
            ->assertJson([
                'data' => null,
                'message' => "{\"username\":[\"The username field must be a string.\"]}",
                'status' => 0
            ]);
        DB::rollBack();
    }

    public function test_OkDeleteUserTest()
    {
        DB::beginTransaction();
        $id = self::id_user;
        $username = config('api.tests.username');
        $email = config('api.tests.email');
        $password = config('api.tests.password');

        $credentials = ['email' => $email, 'password' => $password];
        $jwt = auth('api')->attempt($credentials);

        $response = $this->json('DELETE', self::URL. '/' . $id, [], ['Accept' => 'application/json', 'HTTP_Authorization' => 'Bearer ' . $jwt]);
        $response->assertStatus(200)
            ->assertJson([
                'data' => [
                    "username" => $username,
                    "email" => $email,
                    "id" => $id
                ],
                'status' => 1
            ]);
        DB::rollBack();
    }
    public function test_OkDeleteUserTest2()
    {
        DB::beginTransaction();
        $id = self::id_user;
        $email = config('api.tests.email');
        $password = config('api.tests.password');

        $credentials = ['email' => $email, 'password' => $password];
        $jwt = auth('api')->attempt($credentials);

        $this->json('DELETE', self::URL. '/' . $id, [],['Accept' => 'application/json', 'HTTP_Authorization' => 'Bearer ' . $jwt]);

        $get_user = User::where('id', $id)->first();

        $this->assertTrue(is_null($get_user));
        DB::rollBack();
    }

    public function test_KoDeleteUserTest()
    {
        DB::beginTransaction();
        $id = "fake";
        $email = config('api.tests.email');
        $password = config('api.tests.password');

        $credentials = ['email' => $email, 'password' => $password];
        $jwt = auth('api')->attempt($credentials);

        $response = $this->json('DELETE', self::URL. '/' . $id, [], ['Accept' => 'application/json', 'HTTP_Authorization' => 'Bearer ' . $jwt]);
        $response->assertStatus(422)
            ->assertJson([
                'data' => null,
                'message' => 'Wrong id given',
                'status' => 0
            ]);
        DB::rollBack();
    }
}
