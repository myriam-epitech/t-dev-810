<?php

namespace Tests\Feature;

// use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Cache;
use Tests\TestCase;

class DirectionTest extends TestCase
{
    protected const route = '/api/location/direction';

    protected const mode = "driving";
    protected const newLocationSearch = [
        "location" => "lyon"
    ];
    protected const arrayOfCoordinates = [
        [
            "longitude" => -74.198450,
            "latitude" => 40.776968
        ],
        [
            "longitude" => -74.174420,
            "latitude" => 40.856467
        ]
    ];

    public function test_OKGetDirection()
    {
        $response = $this->postJson(self::route, ["coordinates" => self::arrayOfCoordinates, "mode" => self::mode], ['Accept' => 'application/json']);

        $array = $response->original;
        $response->assertJsonStructure([
            'data',
            'message',
            'status'
        ])
            ->assertStatus(200);
        $this->assertEquals(1, $array["status"]);
    }

    public function test_KOGetDirectionNoMode()
    {
        $response = $this->postJson(self::route, ["coordinates" => self::arrayOfCoordinates], ['Accept' => 'application/json']);

        $array = $response->original;
        $response->assertJsonStructure([
            'data',
            'message',
            'status'
        ])
            ->assertJson([
                'data' => null,
                'message' => "No mode given",
                'status' => 0
            ])
            ->assertStatus(422);
        $this->assertEquals(0, $array["status"]);
    }

    public function test_KOGetDirectionNoCoordinates()
    {
        $response = $this->postJson(self::route, ["mode" => self::mode], ['Accept' => 'application/json']);

        $array = $response->original;
        $response->assertJsonStructure([
            'data',
            'message',
            'status'
        ])
            ->assertJson([
                'data' => null,
                'message' => "No coordinates given",
                'status' => 0
            ])
            ->assertStatus(422);
        $this->assertEquals(0, $array["status"]);
    }

    public function test_KOGetDirectionNotEnoughCoordinates()
    {
        $response = $this->postJson(self::route, ["coordinates" => [self::arrayOfCoordinates[0]], "mode" => self::mode], ['Accept' => 'application/json']);

        $array = $response->original;
        $response->assertJsonStructure([
            'data',
            'message',
            'status'
        ])
            ->assertJson([
                'data' => null,
                'message' => "Not enough coordinates given",
                'status' => 0
            ])
            ->assertStatus(422);
        $this->assertEquals(0, $array["status"]);
    }
}
