<?php

namespace Tests\Feature;

// use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class AuthTest extends TestCase
{
    use DatabaseMigrations;
    protected const route_login = '/api/login';
    protected const route_logout = '/api/logout';

    protected function setUp(): void
    {
        parent::setUp();
        User::factory()->create([
            'id' => 0,
            'username' => config('api.tests.username'),
            'email' => config('api.tests.email'),
            'password' => Hash::make(config('api.tests.password')),
        ]);
    }

    public function testLogin()
    {

        $email = config('api.tests.email');
        $password = config('api.tests.password');

        $response = $this->json('POST', self::route_login , [
            'email' => $email,
            'password' => $password
        ]);
        $array = $response->original;

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'data'=> [
                    'token',
                    'user'
                ],
                'message',
                'status'
            ]);
        $this->assertEquals(1, $array["status"]);
    }

    public function testKoLogin()
    {

        $email = config('api.tests.email');

        $response = $this->json('POST', self::route_login , [ 'email' => $email ]);

        $response
            ->assertStatus(422)
            ->assertExactJson([
                "data"=> null,
                "message"=> "Missing credentials",
                "status"=> 0
            ]);
    }

    public function testKo2Login()
    {

        $email = config('api.tests.email');
        $password = config('api.tests.email');

        $response = $this->json('POST', self::route_login , [ 'email' => $email, 'password' => $password ]);

        $response
            ->assertStatus(401)
            ->assertExactJson([
                "data"=> null,
                "message"=> "Unauthorized: invalid credentials",
                "status"=> 0
            ]);
    }

    public function testLogout()
    {
        $email = config('api.tests.email');
        $password = config('api.tests.password');

        $credentials = ['email' => $email, 'password' => $password];
        $jwt = auth('api')->attempt($credentials);

        $response = $this->json('GET', self::route_logout, [], ['HTTP_Authorization' => 'Bearer ' . $jwt]);

        $array = $response->original;
        $response
            ->assertStatus(200)
            ->assertExactJson([
                "data"=> "Logged out successfully",
                "message"=> "Logged out successfully",
                "status"=> 1
            ]);
        $this->assertEquals(1, $array["status"]);
    }

    public function testKoLogout()
    {
        $response = $this->json('GET', self::route_logout, []);

        $response
            ->assertStatus(200)
            ->assertExactJson([
                "data"=> null,
                "message"=> "Authorization Token not found",
                "status"=> 0
            ]);
    }
}
