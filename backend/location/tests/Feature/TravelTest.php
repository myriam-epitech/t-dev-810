<?php

namespace Tests\Feature;

// use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class TravelTest extends TestCase
{
    use DatabaseMigrations;
    protected const routeCreate = '/api/travel/';
    protected const body = [
        "name" => "paris",
        "pois" => [
            ["name" => "pois1", "type" => "pois"],
            ["name" => "pois2", "type" => "restaurant"]
        ]
    ];
    protected const wrongUserId = -1;
    protected function setUp(): void
    {
        parent::setUp();
        User::factory()->create([
            'id' => 0,
            'username' => config('api.tests.username'),
            'email' => config('api.tests.email'),
            'password' => Hash::make(config('api.tests.password')),
        ]);
    }

    public function testOkGetTravel() {
        $id = 0;
        $email = config('api.tests.email');
        $password = config('api.tests.password');

        $credentials = ['email' => $email, 'password' => $password];
        $jwt = auth('api')->attempt($credentials);

        $response1 = $this->json('POST', self::routeCreate . $id , self::body, ['HTTP_Authorization' => 'Bearer ' . $jwt]);
        $array = $response1->original;
        $id_travel = json_decode($response1->getContent())->data->id;

        $response2 = $this->json('GET', "/api/travel/" . $id_travel , [], ['HTTP_Authorization' => 'Bearer ' . $jwt]);
        $response2->assertStatus(200)
            ->assertJsonStructure([
                "data"=> [],
                "message",
                "status"
            ]);
        $this->assertEquals(1, $array["status"]);
    }

    public function testKoGetTravel() {
        $id = 0;
        $email = config('api.tests.email');
        $password = config('api.tests.password');

        $credentials = ['email' => $email, 'password' => $password];
        $jwt = auth('api')->attempt($credentials);

        $response1 = $this->json('POST', self::routeCreate . $id , self::body, ['HTTP_Authorization' => 'Bearer ' . $jwt]);
        $array = $response1->original;
        $id_travel =-1;

        $response2 = $this->json('GET', "/api/travel/" . $id_travel , [], ['HTTP_Authorization' => 'Bearer ' . $jwt]);
        $response2->assertStatus(422)
            ->assertJson([
                'data' => null,
                'message' => 'Wrong travel plan id given',
                'status' => 0
            ]);
        $this->assertEquals(1, $array["status"]);
    }

    public function testOkGetTravels() {
        $id = 0;
        $email = config('api.tests.email');
        $password = config('api.tests.password');

        $credentials = ['email' => $email, 'password' => $password];
        $jwt = auth('api')->attempt($credentials);

        $response1 = $this->json('POST', self::routeCreate . $id , self::body, ['HTTP_Authorization' => 'Bearer ' . $jwt]);
        $array = $response1->original;
        $id_travel = json_decode($response1->getContent())->data->id;

        $response2 = $this->json('GET', "/api/user/" . $id . "/travels" , [], ['HTTP_Authorization' => 'Bearer ' . $jwt]);
        $response2->assertStatus(200)
            ->assertJsonStructure([
                "data"=> [],
                "message",
                "status"
            ]);
        $this->assertEquals(1, $array["status"]);
    }
    public function testKoGetTravels() {
        $id = 0;
        $email = config('api.tests.email');
        $password = config('api.tests.password');

        $credentials = ['email' => $email, 'password' => $password];
        $jwt = auth('api')->attempt($credentials);

        $response1 = $this->json('POST', self::routeCreate . $id , self::body, ['HTTP_Authorization' => 'Bearer ' . $jwt]);
        $array = $response1->original;
        $id_travel = json_decode($response1->getContent())->data->id;

        $response2 = $this->json('GET', "/api/user/" . self::wrongUserId  . "/travels" , [], ['HTTP_Authorization' => 'Bearer ' . $jwt]);
        $response2->assertStatus(422)
            ->assertExactJson([
                "data"=> null,
                "message"=> "Wrong user id given",
                "status"=> 0
            ]);
    }
    public function testOkCreateTravel()
    {
        $id = 0;
        $email = config('api.tests.email');
        $password = config('api.tests.password');

        $credentials = ['email' => $email, 'password' => $password];
        $jwt = auth('api')->attempt($credentials);

        $response = $this->json('POST', self::routeCreate . $id , self::body, ['HTTP_Authorization' => 'Bearer ' . $jwt]);

        $array = $response->original;
        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                "data"=> [
                    "can_share",
                    "user_id",
                    "id",
                    "created_at",
                    "updated_at",
                ],
                "message",
                "status"
            ]);
        $this->assertEquals(1, $array["status"]);
    }

    public function testKoCreateTravel()
    {
        $email = config('api.tests.email');
        $password = config('api.tests.password');

        $credentials = ['email' => $email, 'password' => $password];
        $jwt = auth('api')->attempt($credentials);

        $response = $this->json('POST', self::routeCreate . self::wrongUserId , self::body, ['HTTP_Authorization' => 'Bearer ' . $jwt]);

        $array = $response->original;
        $response
            ->assertStatus(422)
            ->assertExactJson([
                "data"=> null,
                "message"=> "Wrong user id given",
                "status"=> 0
            ]);
        $this->assertEquals(0, $array["status"]);
    }

    public function testOkUpdateTravel()
    {
        $userId = 0;
        $email = config('api.tests.email');
        $password = config('api.tests.password');

        $credentials = ['email' => $email, 'password' => $password];
        $jwt = auth('api')->attempt($credentials);

        $response1 = $this->json('POST', self::routeCreate . $userId , self::body, ['HTTP_Authorization' => 'Bearer ' . $jwt]);
        $id_travel = json_decode($response1->getContent())->data->id;
        $can_share = json_decode($response1->getContent())->data->can_share;

        $response2 = $this->json('PATCH', self::routeCreate . $id_travel , ["can_share" => !$can_share], ['HTTP_Authorization' => 'Bearer ' . $jwt]);
        $array = $response2->original;
        $response2
            ->assertStatus(200)
            ->assertJsonStructure([
                "data"=> [
                    "can_share",
                    "user_id",
                    "id",
                    "created_at",
                    "updated_at",
                ],
                "message",
                "status"
            ]);
        $this->assertEquals(1, $array["status"]);
        $this->assertEquals(json_decode($response1->getContent())->data->can_share, !json_decode($response2->getContent())->data->can_share);
    }

    public function testKoUpdateTravel()
    {
        $userId = 0;
        $email = config('api.tests.email');
        $password = config('api.tests.password');

        $credentials = ['email' => $email, 'password' => $password];
        $jwt = auth('api')->attempt($credentials);

        $response1 = $this->json('POST', self::routeCreate . $userId , self::body, ['HTTP_Authorization' => 'Bearer ' . $jwt]);
        $id_travel = -1;
        $can_share = json_decode($response1->getContent())->data->can_share;

        $response2 = $this->json('PATCH', self::routeCreate . $id_travel , ["can_share" => !$can_share], ['HTTP_Authorization' => 'Bearer ' . $jwt]);
        $array = $response2->original;
        $response2
            ->assertStatus(422)
            ->assertJson([
                'data' => null,
                'message' => 'Wrong travel plan id given',
                'status' => 0
            ]);
        $this->assertEquals(0, $array["status"]);
    }

    public function testOkDeleteTravel()
    {
        $userId = 0;
        $email = config('api.tests.email');
        $password = config('api.tests.password');

        $credentials = ['email' => $email, 'password' => $password];
        $jwt = auth('api')->attempt($credentials);

        $response1 = $this->json('POST', self::routeCreate . $userId , self::body, ['HTTP_Authorization' => 'Bearer ' . $jwt]);
        $id_travel = json_decode($response1->getContent())->data->id;

        $response2 = $this->json('DELETE', self::routeCreate . $id_travel , [], ['HTTP_Authorization' => 'Bearer ' . $jwt]);
        $array = $response2->original;
        $response2
            ->assertStatus(200)
            ->assertJsonStructure([
                'data',
                'message',
                'status'
            ]);;
        $this->assertEquals(1, $array["status"]);
        $this->assertEquals( 'Travel plan deleted', $array["message"]);
    }

    public function testKoDeleteTravel()
    {
        $userId = 0;
        $email = config('api.tests.email');
        $password = config('api.tests.password');

        $credentials = ['email' => $email, 'password' => $password];
        $jwt = auth('api')->attempt($credentials);

        $response1 = $this->json('POST', self::routeCreate . $userId , self::body, ['HTTP_Authorization' => 'Bearer ' . $jwt]);
        $id_travel = -1;

        $response2 = $this->json('DELETE', self::routeCreate . $id_travel , [], ['HTTP_Authorization' => 'Bearer ' . $jwt]);
        $array = $response2->original;
        $response2
        ->assertStatus(422)
        ->assertJson([
            'data' => null,
            'message' => 'Wrong travel plan id given',
            'status' => 0
        ]);
        $this->assertEquals(0, $array["status"]);
    }
}
