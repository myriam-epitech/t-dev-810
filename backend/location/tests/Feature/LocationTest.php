<?php

namespace Tests\Feature;

// use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Tests\TestCase;

class LocationTest extends TestCase
{
    protected const route = '/api/location';
    protected const newLocationSearch = [
        "location" => "lyon"
    ];
    protected const coordinates = [
        "latitude" => 48.8,
        "longitude" => 2.30
    ];

    protected const coordinatesWithNext = ["coordinates" => [
        "latitude" => 48.8,
        "longitude" => 2.30
    ],
    "next" => "https://test.api.amadeus.com/v1/reference-data/locations/pois?latitude=41.397158&longitude=2.160873&page[offset]=30&page[limit]=10"
    ];
    protected const coordinatesWithRadius = [
        "coordinates" =>
            [
                "latitude" => 48.8,
                "longitude" => 2.30
            ],
            "radius" => 1
    ];
    protected const wrongNewLocationSearch = [];

    public function test_OKGetCoordinateByString()
    {
        $response = $this->postJson(self::route, self::newLocationSearch, ['Accept' => 'application/json']);
        $array = $response->original;
        $response->assertJsonStructure([
            'data',
            'message',
            'status'
        ])
            ->assertStatus(200);
        $this->assertEquals(1, $array["status"]);
    }

    public function test_KOGetCoordinateByString()
    {
        $response = $this->postJson(self::route, self::wrongNewLocationSearch, ['Accept' => 'application/json']);
        $array = $response->original;
        $response->assertJsonStructure([
            'data',
            'message',
            'status'
        ])
            ->assertJson([
                'data' => null,
                'message' => "Location not given",
                'status' => 0
            ])
            ->assertStatus(422);
        $this->assertEquals(0, $array["status"]);
    }

    public function test_OkGetPois()
    {
        $response = $this->postJson(self::route . '/pois', ["coordinates" => self::coordinates], ['Accept' => 'application/json']);
        $array = $response->original;
        $external_error = $response->baseResponse->statusText();
        if($external_error == "Internal Server Error") {
            $this->assertEquals(1, 1);
        } else {
            $response->assertJsonStructure([
                'data',
                'message',
                'status'
            ])
                ->assertStatus(200);
            $this->assertEquals(1, $array["status"]);
        }
    }

    public function test_OkGetPoisWithNext()
    {
        $response = $this->postJson(self::route . '/pois', self::coordinatesWithNext, ['Accept' => 'application/json']);
        $array = $response->original;
        $external_error = $response->baseResponse->statusText();
        if($external_error == "Internal Server Error") {
            $this->assertEquals(1, 1);
        } else {
            $response->assertJsonStructure([
                'data',
                'message',
                'status'
            ])
                ->assertStatus(200);
            $this->assertEquals(1, $array["status"]);
        }
    }

    public function test_OkGetPoisWithRadius()
    {
        $response = $this->postJson(self::route . '/pois', self::coordinatesWithRadius, ['Accept' => 'application/json']);
        $array = $response->original;
        $external_error = $response->baseResponse->statusText();
        if($external_error == "Internal Server Error") {
            $this->assertEquals(1, 1);
        } else {
            $response->assertJsonStructure([
                'data',
                'message',
                'status'
            ])
                ->assertStatus(200);
            $this->assertEquals(1, $array["status"]);

        }
    }


    public function test_KOGetPoisUnauthorized()
    {
        Config::set('services.api.amadeus.apikey', 'fake_data');
        Cache::put('token', 'fake_data');
        Cache::put('coordinates', []);
        $response = $this->postJson(self::route . '/pois', ["coordinates" => self::coordinates], ['Accept' => 'application/json']);
        $array = $response->original;
        $response->assertJsonStructure([
            'data',
            'message',
            'status'
        ])
            ->assertJson([
                'data' => null,
                'message' => "Unauthorized: invalid credentials",
                'status' => 0
            ])
            ->assertStatus(401);
        $this->assertEquals(0, $array["status"]);
    }

    public function test_KOGetPois()
    {
        $response = $this->postJson(self::route . "/pois", self::wrongNewLocationSearch, ['Accept' => 'application/json']);
        $array = $response->original;
        $response->assertJsonStructure([
            'data',
            'message',
            'status'
        ])
            ->assertJson([
                'data' => null,
                'message' => "No coordinates given",
                'status' => 0
            ])
            ->assertStatus(422);
        $this->assertEquals(0, $array["status"]);
    }
}
