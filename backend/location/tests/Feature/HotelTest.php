<?php

namespace Tests\Feature;

// use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class HotelTest extends TestCase
{
    protected const routeGetHotels = '/api/location/hotels';
    protected const routeGetHotelById = '/api/location/hotelById';
    protected const newLocationSearch = [
        "location" => "lyon"
    ];
    protected const coordinates = [
        "coordinates" => [
            "latitude" => 48.8,
            "longitude" => 2.30
        ]
    ];
    protected const wrongNewLocationSearch = [];
    protected const HotelId = ["hotel_id" => "MCLONGHM"];
    protected const WrongHotelId = ["hotel_id" => "randomId"];
    protected const EmptyHotelId = ["hotel_id" => null];

    public function test_OKGetHotelsByLocation()
    {
        $response = $this->postJson(self::routeGetHotels, self::coordinates, ['Accept' => 'application/json']);
        $array = $response->original;
        $response->assertJsonStructure([
            'data',
            'message',
            'status'
        ])
            ->assertStatus(200);
        $this->assertEquals(1, $array["status"]);
    }

    public function test_KOGetHotelsByLocation()
    {
        $response = $this->postJson(self::routeGetHotels, self::wrongNewLocationSearch, ['Accept' => 'application/json']);
        $array = $response->original;
        $response->assertJsonStructure([
            'data',
            'message',
            'status'
        ])
            ->assertJson([
                'data' => null,
                'message' => "No coordinates given",
                'status' => 0
            ])
            ->assertStatus(422);
        $this->assertEquals(0, $array["status"]);
    }

    public function test_OKGetHotelbyId()
    {
        $response = $this->postJson(self::routeGetHotelById, self::HotelId, ['Accept' => 'application/json']);
        $array = $response->original;
        $response->assertJsonStructure([
            'data',
            'message',
            'status'
        ])
            ->assertStatus(200);
        $this->assertEquals(1, $array["status"]);
    }



    public function test_KoGetHotelByIdEmptyId()
    {
        $response = $this->postJson(self::routeGetHotelById, self::EmptyHotelId, ['Accept' => 'application/json']);
        $array = $response->original;
        $response->assertJsonStructure([
            'data',
            'message',
            'status'
        ])
            ->assertJson([
                'data' => null,
                'message' => "Hotel id not given",
                'status' => 0
            ])
            ->assertStatus(422);
        $this->assertEquals(0, $array["status"]);
    }

    public function test_KoGetHotelByIdWrongId()
    {
        $response = $this->postJson(self::routeGetHotelById, self::WrongHotelId, ['Accept' => 'application/json']);
        $array = $response->original;
        $response->assertJsonStructure([
            'data',
            'message',
            'status'
        ])
            ->assertJson([
                'data' => null,
                'message' => "Error in the request",
                'status' => 0
            ])
            ->assertStatus(400);
        $this->assertEquals(0, $array["status"]);
    }
}
