<?php

namespace Tests\Unit;

use App\Models\Hotel;
use App\Models\Pois;
use App\Models\Restaurant;
use App\Models\Transport;
use App\Models\TravelPlan;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Tests\TestCase;

class ModelsTest extends TestCase
{

    use WithFaker, DatabaseMigrations;

    /** @test  */

    public function test_poisHasColumn()
    {
        $this->assertTrue(
            Schema::hasColumns('pois', [
                'id',
                'name',
                'type',
                'travelplan_id',
                'latitude',
                'longitude',
                'address',
                'other'
            ]), 1);
    }

    public function test_usersHasColumn()
    {
        $this->assertTrue(
            Schema::hasColumns('users', [
                'username',
                'email',
                'password',
            ]), 1);
    }

    public function test_travelplansHasColumn()
    {
        $this->assertTrue(
            Schema::hasColumns('travel_plans', [
                "id",
                "user_id",
                "can_share",
                "name"
            ]), 1);
    }


    public function test_PoisBelongsToTravelPlan()
    {
        DB::beginTransaction();
        $user = User::factory()->create();
        $travelPlan = TravelPlan::factory()->create([
            'user_id' => $user->id
        ]);
        $pois = Pois::factory()->create(['travelplan_id' => $travelPlan->id]);
        $this->assertEquals($travelPlan->id, $pois->travelplan_id);
        $this->assertInstanceOf(TravelPlan::class, $pois->travelPlan);
        $this->assertEquals($travelPlan->id, $pois->travelplan_id);
        DB::rollBack();
    }

    public function test_TravelPlanBelongsToUser()
    {
        DB::beginTransaction();
        $user = User::factory()->create();
        $travelPlan = TravelPlan::factory()->create([
            'user_id' => $user->id
        ]);
        $this->assertEquals($user->id, $travelPlan->user_id);
        $this->assertInstanceOf(User::class, $travelPlan->user);
        $this->assertEquals($user->id, $travelPlan->user_id);
        DB::rollBack();
    }

    public function test_TravelPlanHasManyPois()
    {
        DB::beginTransaction();
        $user = User::factory()->create();
        $travelPlan = TravelPlan::factory()->create([
            'user_id' => $user->id
        ]);
        $pois = Pois::factory()->create(['travelplan_id' => $travelPlan->id]);

        $this->assertInstanceOf(Collection::class, $travelPlan->pois);
        $this->assertEquals($pois->id, $travelPlan->pois->first()->id);
        $this->assertEquals(1, $travelPlan->pois->count());

        DB::rollBack();
    }


    public function test_UserHasManyTravelPlans()
    {
        DB::beginTransaction();
        $user = User::factory()->create();
        $travelPlan = TravelPlan::factory()->create([
            'user_id' => $user->id
        ]);

        $this->assertInstanceOf(Collection::class, $user->travelPlans);
        $this->assertEquals($travelPlan->id, $user->travelPlans->first()->id);
        $this->assertEquals(1, $user->travelPlans->count());

        DB::rollBack();
    }
}
