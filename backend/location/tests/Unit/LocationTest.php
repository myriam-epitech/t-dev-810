<?php

namespace Tests\Unit;

use App\Http\Controllers\LocationController;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Cache;
use Tests\TestCase;
use Helper;

class LocationTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();

        // Load the .env file
        Artisan::call('config:cache');
    }

    public function test_OKGetAmadeusToken()
    {
        $client_id = config('services.api.amadeus.apikey');
        $client_secret = config('services.api.amadeus.secret');

        $token_before = Cache::get('token');
        $this->assertEquals(null,  $token_before);

        $tested = Helper::getAmadeusToken($client_id,  $client_secret);
        $token_after = Cache::get('token');

        $this->assertEquals(true,  is_string($token_after));
        $this->assertEquals(true,  is_string($tested));
    }
    public function test_KOGetAmadeusToken()
    {
        $tested = Helper::getAmadeusToken('fake_data', 'fake_data');
        $this->assertEquals(401,  $tested);
    }
}
