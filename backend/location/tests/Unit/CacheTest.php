<?php

namespace Tests\Unit;

use App\Http\Controllers\LocationController;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Cache;
use Tests\TestCase;
use Helper;
use function PHPUnit\Framework\assertEquals;

class CacheTest extends TestCase
{
    protected const coordinates = [
        "latitude" => 48.8,
        "longitude" => 2.30
    ];

    protected const emptyCoordinates = [];

    protected const pois = [
        "pois" => [
                "name" => "Casa Batlló",
                "category" => "SIGHTS",
                "tags" => [
                    "sightseeing",
                    "sights",
                    "museum",
                    "landmark",
                    "tourguide",
                    "restaurant",
                    "attraction",
                    "activities",
                    "commercialplace",
                    "shopping",
                    "souvenir"
                ],
                "coordinates" => [
                    "latitude" => 41.39165,
                    "longitude" => 2.164772
                ]
        ],
        "meta" => [
            "count" => 120,
            "links" => [
                "self" => "https://test.api.amadeus.com/v1/reference-data/locations/pois?latitude=41.397158&longitude=2.160873",
                "next" => "https://test.api.amadeus.com/v1/reference-data/locations/pois?latitude=41.397158&longitude=2.160873&page[offset]=10&page[limit]=10",
                "last" => "https://test.api.amadeus.com/v1/reference-data/locations/pois?latitude=41.397158&longitude=2.160873&page[offset]=120&page[limit]=10",
                "first" => "https://test.api.amadeus.com/v1/reference-data/locations/pois?latitude=41.397158&longitude=2.160873&page[offset]=0&page[limit]=10",
                "up" => "https://test.api.amadeus.com/v1/reference-data/locations/pois?latitude=41.397158&longitude=2.160873"
            ]
        ]
    ];

    protected function setUp(): void
    {
        parent::setUp();

        // Load the .env file
        Artisan::call('config:cache');
    }

    public function test_OKGetAmadeusToken()
    {
        $client_id = config('services.api.amadeus.apikey');
        $client_secret = config('services.api.amadeus.secret');

        $token_before = Cache::get('token');
        $this->assertEquals(null,  $token_before);

        $tested = Helper::getAmadeusToken($client_id,  $client_secret);
        $token_after = Cache::get('token');
        $this->assertTrue(is_string($token_after));
        $this->assertTrue(is_string($tested));
    }
    public function test_KOGetAmadeusToken()
    {
        $tested = Helper::getAmadeusToken('fake_data', 'fake_data');
        $this->assertEquals(401,  $tested);
    }

    public function test_setCacheLocation() {
        Helper::setCacheLocation(self::coordinates);
        $this->assertEquals(self::coordinates, Cache::get('coordinates'));
    }

    public function test_getCacheLocation() {
        Helper::setCacheLocation(self::coordinates);
        $this->assertEquals(self::coordinates, Helper::getCacheLocation());
    }

    public function test_OKcheckCacheLocationChanges() {
        $tested = Helper::checkCacheLocationChanges(self::coordinates);
        $this->assertEquals(1, $tested);
    }

    public function test_KOcheckCacheLocationChanges() {
        $tested = Helper::checkCacheLocationChanges(self::emptyCoordinates);
        $this->assertEquals(0, $tested);
    }

    public function test_setCachePois() {
        Helper::setCachePois(self::pois);
        $this->assertEquals(self::pois, Cache::get('pois'));
    }

    public function test_getCachePois() {
        Helper::setCachePois(self::pois);
        $this->assertEquals(self::pois, Helper::getCachePois());
    }
}
