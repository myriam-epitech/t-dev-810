<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TravelPlan extends Model
{
    use HasFactory;

    protected $fillable = [
        "id",
        "user_id",
        "can_share",
        "name",
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function pois()
    {
        return $this->hasMany(Pois::class, 'travelplan_id');
    }

}
