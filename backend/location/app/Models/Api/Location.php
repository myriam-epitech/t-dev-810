<?php

namespace App\Models\Api;

class Location
{
    public $bbox;
    public $latitude;
    public $longitude;
    public $place_name;

    function __construct($data) {
        $this->place_name = $data->place_name;
        $this->longitude = current($data->geometry->coordinates);
        $this->latitude = end($data->geometry->coordinates);
        $this->bbox = property_exists($data, 'bbox') ? $data->bbox : null;
    }
}
