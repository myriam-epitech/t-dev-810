<?php

namespace App\Models\Api;

class Hotels
{
    public $id;
    public $name;
    public $latitude;
    public $longitude;
    public $distance;

    function __construct($data) {
        $this->id = $data->hotelId;
        $this->name = $data->name;
        $this->latitude = $data->geoCode->latitude;
        $this->longitude = $data->geoCode->longitude;
        $this->distance = $data->distance;
    }
}
