<?php

namespace App\Models\Api;

class Hotel
{
    public $name;
    public $hotelId;
    public $latitude;
    public $longitude;

    public $offers = [];

    public $distance;

    function __construct($data) {
        $this->name = $data->hotel->name;
        $this->hotelId = $data->hotel->hotelId;
        $this->latitude = $data->hotel->latitude;
        $this->longitude = $data->hotel->longitude;
        $this->available = $data->available;
        $this->offers = $data->offers;
    }
}
