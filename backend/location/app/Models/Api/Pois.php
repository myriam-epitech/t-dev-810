<?php

namespace App\Models\Api;

class Pois
{
    public $name;
    public $category;
    public $tags;
    public $coordinates;
    function __construct($data) {
        $this->name = $data->name;
        $this->category = $data->category;
        $this->tags = $data->tags;
        $this->coordinates = $data->geoCode;
    }
}
