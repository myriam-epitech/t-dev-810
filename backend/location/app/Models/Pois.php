<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pois extends Model
{
    use HasFactory;

    protected $fillable = [
        "id",
        "name",
        "type",
        "travelplan_id",
        "latitude",
        "longitude",
        "address",
        "other"
    ];

    public function travelPlan()
    {
        return $this->belongsTo(TravelPlan::class, 'travelplan_id');
    }
}
