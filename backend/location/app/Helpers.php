<?php

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use Illuminate\Support\Facades\Cache;
class Helper
{
    public static function formatData($data, $class): array
    {
        $array = [];

        foreach($data as $feature){
            $loc = new $class($feature);
            $array[] = $loc;
        }
        return $array;
    }

    public static function formatAmadeusResponse(Response $response) {
        $res = json_decode($response->getBody()->getContents());
        return $res;
    }

    public static function formatUrl(Array $coordinates, String $mode): string
    {
        $url = config('services.api.mapbox.directionUrl') . $mode . '/';
        foreach ($coordinates as $key => $coordinate) {
            $url .= $coordinate['longitude'] . ',' . $coordinate['latitude'];
            if ($key < count($coordinates) - 1) {
                $url .= ';';
            }
        }
        return $url;
    }

    public static function formatUrlWithFilters($path, $filters) {
        if (empty($filters)) return $path;
        foreach ($filters as $key => $filter) {
            if (gettype($filter) == 'array') {
                $filter = '"' . implode(',', $filter) . '"';
            }
            $path .= '&' . $key . '=' . $filter;
        }
        return $path;
    }

    public static function getAmadeusToken($client_id, $client_secret)
    {
        try {
            $client = new Client();
            $headers = [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ];

            $options = [
                'form_params' => [
                    'grant_type' => 'client_credentials',
                    'client_id' => $client_id,
                    'client_secret' => $client_secret
                ],
                'headers' => $headers
            ];
            $response = $client->request('POST', config('services.api.amadeus.urlV1') . '/security/oauth2/token', $options);
            $result = json_decode($response->getBody()->getContents());

            Cache::put('token', $result->access_token, $result->expires_in);

            return $result->access_token;
        } catch (\Exception $ex) {
            return $ex->getCode();
        }
    }

    public static function getCacheToken() {
        $token = Cache::get('token');
        if (!$token) {
            $token = self::getAmadeusToken(config('services.api.amadeus.apikey'), config('services.api.amadeus.secret'));
        }
        return $token;
    }

    public static function setCacheLocation($coordinates): void
    {
        Cache::put("coordinates", $coordinates);
    }

    public static function getCacheLocation() {
        $coordinates = Cache::get('coordinates');
        if (!$coordinates) $coordinates = [];
        return $coordinates;
    }

    public static function checkCacheLocationChanges(Array $coordinates): int
    {
        $cacheCoordinates = self::getCacheLocation();
        $diff = array_diff($coordinates, $cacheCoordinates);
        if (!empty($diff)) {
            self::setCacheLocation($coordinates);
            return 1;
        }
        return 0;
    }

    public static function setCachePois($pois): void
    {
        Cache::put('pois', $pois);
    }

    public static function getCachePois() {
        $pois = Cache::get('pois');
        if (!$pois) $pois = [];
        return $pois;
    }
}
