<?php

namespace App\Http\Controllers;

use App\Models\Location;
use App\Models\Pois;
use Exception;
use GuzzleHttp\Client;
use http\Env;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Cache;
use Helper;
use OpenApi\Annotations as OA;

class DirectionController extends Controller
{
    /**
     * @OA\Post(
     *     path="/api/location/direction",
     *     operationId="GetDirection",
     *     tags={"Direction"},
     *     @OA\RequestBody(
     *     required=true,
     *     description="Get a direction by coordinates",
     *     @OA\JsonContent(
     *     required={"coordinates", "mode"},
     *     @OA\Property(property="coordinates", type="object", example={{"latitude": "2.3522", "longitude": "48.8566"},{"latitude": "2.2945", "longitude" : "48.8584"}}),
     *     @OA\Property(property="mode", type="string", example="driving")
     *   )
     * ),
     *     @OA\Response(response="200", description="Get a direction by coordinates"),
     *     @OA\Response(response="422", description="Wrong parameter")
     * )
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDirection(Request $request) {
        try {
            if (!$request->get('coordinates')) {
                throw new Exception("No coordinates given", 422);
            }
            if (!$request->get('mode')) {
                throw new Exception("No mode given", 422);
            }
            $arrayOfCoordinates = $request->get('coordinates');
            if (gettype($arrayOfCoordinates) != "array" || count($arrayOfCoordinates) < 2) {
                throw new Exception("Not enough coordinates given", 422);
            }
            $mode = $request->get('mode');

            // Use of function to format url
            $url = Helper::formatUrl($arrayOfCoordinates, $mode);
            $path = $url . "?geometries=geojson&access_token=" . config('services.api.mapbox.pubkey');
            $response = Http::get($path);
//            $result = Helper::formatData($response->object()->features, Location::class);

            return $this->returnWithSuccess($response);
        } catch (\Exception $ex) {
            return $this->returnWithError($ex);
        }
    }
}
