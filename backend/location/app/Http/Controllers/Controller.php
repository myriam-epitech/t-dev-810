<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

/**
 * @OA\Info(title="API Documentation", version="1.0")
 */
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    function returnWithError(Exception $ex, $customize=null)
    {
        $http_code = 500;
        $msg=$ex->getMessage();

        if($ex->getCode() == 422 || $ex->getCode() == 23000){
            $http_code = 422;
        }
        if($ex->getCode() == 400){
            $http_code = 400;
            $msg="Error in the request";
        }
        if($ex->getCode() == 401){
            $http_code = 401;
            $msg="Unauthorized: invalid credentials";
        }
        if($ex->getCode() == 503){
            $http_code = 503;
            $msg="No available service! Try later";
        }
        if ($customize !== null) {
            $http_code = $customize['code'];
            $msg = $customize['msg'];
        }
        return response()->json(['data' => null, 'message' => $msg, 'status' => 0], $http_code);
    }

    function returnWithSuccess($result = null, $message = "")
    {
        $http_code = 200;

        return response()->json(['data' => $result, 'message' => $message, 'status' => 1], $http_code);

    }
}
