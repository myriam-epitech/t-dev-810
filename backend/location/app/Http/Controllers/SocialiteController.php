<?php

namespace App\Http\Controllers;
use App\Models\User;
use Illuminate\Http\Request;
use Helper;
use Laravel\Socialite\Facades\Socialite;
use Tymon\JWTAuth\Facades\JWTAuth;

class SocialiteController extends Controller
{
    public function redirect () {
        return Socialite::driver('google')->stateless()->redirect();
    }

    public function callback () {

        $data = Socialite::driver('google')->stateless()->user();
        $email = $data->getEmail();
        $name = $data->getName();

        $user = User::where("email", $email)->first();

        if (!isset($user)) {
            $user = User::create([
                'username' => $name,
                'email' => $email,
                'password' => bcrypt("password")
            ]);
        }
        $token = JWTAuth::fromUser($user);
        $cookie = cookie('token', $token, 3600);

        return $this->returnWithSuccess(["token"=> $token, "user" => $user], "User connected")->cookie($cookie)->header('Authorization', $token);
//        return $this->redirect("http://localhost:3000")->cookie($cookie)->header('Authorization', $token);
    }
}
