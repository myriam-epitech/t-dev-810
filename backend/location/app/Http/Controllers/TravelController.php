<?php

namespace App\Http\Controllers;

use App\Models\Hotel;
use App\Models\Pois;
use App\Models\Restaurant;
use App\Models\Transport;
use App\Models\TravelPlan;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Exception;
use OpenApi\Annotations as OA;

class TravelController extends Controller
{
    /**
     * @OA\Get(
     *     path="/api/travel/{id}",
     *     operationId="Get travel plan",
     *     tags={"Travel plan"},
     *     @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="Travel plan id",
     *     required=true,
     *     @OA\Schema(
     *     type="integer",
     *     format="int64"
     *   )
     * ),
     *     @OA\Response(
     *     response="200",
     *     description="Get travel plan",
     *     @OA\JsonContent(
     *     @OA\Property(property="id", type="integer", example="1"),
     *     @OA\Property(property="user_id", type="integer", example="1"),
     *     @OA\Property(property="name", type="string", example="name"),
     *     @OA\Property(property="description", type="string", example="description"),
     *     @OA\Property(property="created_at", type="string", example="2021-05-18T15:00:00.000000Z"),
     *     @OA\Property(property="updated_at", type="string", example="2021-05-18T15:00:00.000000Z"),
     *     @OA\Property(property="pois", type="object", example="pois")
     * )
     * ),
     *     @OA\Response(response="422", description="Wrong travel plan id given"),
     *     @OA\Response(response="404", description="Travel plan not found")
     * )
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTravelPlan($id) {
        try {
            $travel_plan = TravelPlan::find($id);
            if(!$travel_plan){
                throw new Exception("Wrong travel plan id given", 422);
            }

            $result = TravelPlan::with('pois')->where('id', $id)->first();

            return $this->returnWithSuccess($result, "Results fetched with success");

        }catch(Exception $e ){
            DB::rollBack();
            return $this->returnWithError($e);
        }
    }

    /**
     * @OA\Get(
     *     path="/api/user/{id}/travels",
     *     operationId="Get travel plan by user id",
     *     tags={"Travel plan"},
     *     @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="User id",
     *     required=true,
     *     @OA\Schema(
     *     type="integer",
     *     format="int64"
     *  )
     * ),
     *     @OA\Response(
     *     response="200",
     *     description="Get travel plan by user id",
     *     @OA\JsonContent(
     *     @OA\Property(property="id", type="integer", example="1"),
     *     @OA\Property(property="user_id", type="integer", example="1"),
     *     @OA\Property(property="name", type="string", example="name"),
     *     @OA\Property(property="description", type="string", example="description"),
     *     @OA\Property(property="created_at", type="string", example="2021-05-18T15:00:00.000000Z"),
     *     @OA\Property(property="updated_at", type="string", example="2021-05-18T15:00:00.000000Z"),
     *     @OA\Property(property="pois", type="object", example="pois")
     * )
     * ),
     *     @OA\Response(response="422", description="Wrong user id given"),
     *     @OA\Response(response="404", description="User not found")
     * )
     *
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTravelsByUserId($id) {
        try {
            $user = User::find($id);
            if(!$user){
                throw new Exception("Wrong user id given", 422);
            }

            $result = TravelPlan::with('pois')->where('user_id', $id)->get();

            return $this->returnWithSuccess($result, "Results fetched with success");

        }catch(Exception $e ){
            DB::rollBack();
            return $this->returnWithError($e);
        }
    }

    /**
     * @OA\Post(
     *     path="/api/travel/{userId}",
     *     operationId="Create travel plan",
     *     tags={"Travel plan"},
     *     @OA\Parameter(
     *     name="userId",
     *     in="path",
     *     description="User id",
     *     required=true,
     *     @OA\Schema(
     *     type="integer",
     *     format="int64"
     *  )
     * ),
     *     @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *     required={"name"},
     *     @OA\Property(property="name", type="string", example="name"),
     *     @OA\Property(property="can_share", type="boolean", example="true"),
     *     @OA\Property(property="pois", type="array", items="object", example={{"name": "test"}, {"name": "test2"}}),
     *     @OA\Property(property="hotels", type="array", items="string", example={{"name": "test3"}, {"name": "test3"}}),
     *     @OA\Property(property="restaurants", type="array", items="string", example={{"name": "test4"}, {"name": "test5"}}),
     *     @OA\Property(property="transports", type="array", items="string", example={{"name": "test6"}, {"name": "test7"}}),
     *     )
     * ),
     *     @OA\Response(
     *     response="200",
     *     description="Create travel plan",
     *     @OA\JsonContent(
     *     @OA\Property(property="id", type="integer", example="1"),
     *     @OA\Property(property="user_id", type="integer", example="1"),
     *     @OA\Property(property="name", type="string", example="name"),
     *     @OA\Property(property="description", type="string", example="description"),
     *     @OA\Property(property="created_at", type="string", example="2021-05-18T15:00:00.000000Z"),
     *     @OA\Property(property="updated_at", type="string", example="2021-05-18T15:00:00.000000Z"),
     *     @OA\Property(property="pois", type="object", example="pois")
     * )
     * ),
     *     @OA\Response(response="422", description="Wrong user id given"),
     *     @OA\Response(response="404", description="User not found")
     * )
     *
     *
     * @param $userId
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createTravelPlan($userId, Request $request)
    {
        DB::beginTransaction();
        try {
        $user = User::find($userId);
        if(!$user){
            throw new Exception("Wrong user id given", 422);
        }
        $can_share = true;
        if($request->has('can_share')){
            $can_share = $request->get('can_share');
        }

        $validate = Validator::make($request->all(),
            [
                'name' => ['required'],
                'hotels'=> ['array'],
                'pois'=> ['array'],
                'restaurants'=> ['array'],
                'transports'=> ['array'],
            ]);

        $name = $request->get('name');

        if ($validate->fails()) {
            throw new \Exception($validate->errors(), 422);
        }

        $new_travel = TravelPlan::create([
            'user_id' => $userId,
            'can_share' => $can_share,
            'name' => $name,
        ]);


        if($request->has('pois')) {
            $pois = $request->get('pois');
            for($i=0;$i<count($pois); $i++) {
                $new_pois = [
                    'name' => $pois[$i]["name"],
                    'travelplan_id' => $new_travel->id
                ];
                if(array_key_exists("address", $pois[$i]) != null){
                    $new_pois["address"] = $pois[$i]["address"];
                }
                if(array_key_exists("latitude", $pois[$i]) != null){
                    $new_pois["latitude"] = $pois[$i]["latitude"];
                }
                if(array_key_exists("longitude", $pois[$i]) != null){
                    $new_pois["longitude"] = $pois[$i]["longitude"];
                }
                if(array_key_exists("other", $pois[$i]) != null){
                    $new_pois["other"] = $pois[$i]["other"];
                }
                if(array_key_exists("type", $pois[$i]) != null){
                    $new_pois["type"] = $pois[$i]["type"];
                }
                Pois::create($new_pois);

            }
        }

        DB::commit();

        return $this->returnWithSuccess($new_travel, "Travel plan registered");

        }catch(Exception $e ){
            DB::rollBack();
            return $this->returnWithError($e);
        }
    }

    /**
     * @OA\Patch(
     *     path="/api/travel/{id}",
     *     summary="Update travel plan",
     *     tags={"Travel plan"},
     *     @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="Travel plan id",
     *     required=true,
     *     @OA\Schema(
     *     type="integer",
     *     format="int64"
     *    )
     * ),
     *     @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *     required={"name"},
     *     @OA\Property(property="name", type="string", example="name"),
     *     @OA\Property(property="description", type="string", example="description"),
     *     @OA\Property(property="can_share", type="boolean", example="true"),
     *     )
     * ),
     *     @OA\Response(
     *     response="200",
     *     description="Update travel plan",
     *     @OA\JsonContent(
     *     @OA\Property(property="id", type="integer", example="1"),
     *     @OA\Property(property="user_id", type="integer", example="1"),
     *     @OA\Property(property="name", type="string", example="name"),
     *     @OA\Property(property="description", type="string", example="description"),
     *     @OA\Property(property="created_at", type="string", example="2021-05-18T15:00:00.000000Z"),
     *     @OA\Property(property="updated_at", type="string", example="2021-05-18T15:00:00.000000Z"),
     *     @OA\Property(property="pois", type="object", example="pois")
     * )
     * ),
     *     @OA\Response(response="422", description="Wrong travel plan id given"),
     *     @OA\Response(response="404", description="Travel plan not found")
     * )
     *
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateTravelPlan($id, Request $request)
    {
        try {
            $travelPlan = TravelPlan::find($id);
            if(!$travelPlan){
                throw new Exception("Wrong travel plan id given", 422);
            }
            if($request->has('can_share')){
                $can_share = $request->get('can_share');
                $travelPlan->can_share = $can_share;
                $travelPlan->saveOrFail();
            }

            return $this->returnWithSuccess($travelPlan, "Travel plan updated");

        }catch(Exception $e ){
            DB::rollBack();
            return $this->returnWithError($e);
        }
    }

    /**
     * @OA\Delete(
     *     path="/api/travel/{id}",
     *     summary="Delete travel plan",
     *     tags={"Travel plan"},
     *     @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="Travel plan id",
     *     required=true,
     *     @OA\Schema(
     *     type="integer",
     *     format="int64"
     *   )
     * ),
     *     @OA\Response(
     *     response="200",
     *     description="Delete travel plan",
     *     @OA\JsonContent(
     *     @OA\Property(property="id", type="integer", example="1"),
     *     @OA\Property(property="user_id", type="integer", example="1"),
     *     @OA\Property(property="name", type="string", example="name"),
     *     @OA\Property(property="description", type="string", example="description"),
     *     @OA\Property(property="created_at", type="string", example="2021-05-18T15:00:00.000000Z"),
     *     @OA\Property(property="updated_at", type="string", example="2021-05-18T15:00:00.000000Z"),
     *     @OA\Property(property="pois", type="object", example="pois")
     *     )
     * ),
     *     @OA\Response(response="422", description="Wrong travel plan id given"),
     *     @OA\Response(response="404", description="Travel plan not found")
     *
     * )
     *
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteTravelPlan($id, Request $request)
    {
        try {
            $travelPlan = TravelPlan::find($id);
            if(!$travelPlan){
                throw new Exception("Wrong travel plan id given", 422);
            }
            $travelPlan->pois()->delete();
            $travelPlan->delete();
            return $this->returnWithSuccess($travelPlan, "Travel plan deleted");

        }catch(Exception $e ){
            DB::rollBack();
            return $this->returnWithError($e);
        }
    }
}
