<?php

namespace App\Http\Controllers;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use OpenApi\Annotations as OA;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Facades\JWTAuth;
use Exception;
class AuthController extends Controller
{

    /**
     * @OA\Post(
     *     path="/api/login",
     *     operationId="Login",
     *     tags={"Auth"},
     *     @OA\RequestBody(
     *     required=true,
     *     description="Login",
     *     @OA\JsonContent(
     *     required={"email", "password"},
     *     @OA\Property(property="email", type="string", example="email@email.fr"),
     *     @OA\Property(property="password", type="string", example="password")
     *   )
     * ),
     *     @OA\Response(response="200", description="Login"),
     *     @OA\Response(response="422", description="Missing credentials"),
     *     @OA\Response(response="401", description="Invalid Credentials"),
     *     @OA\Response(response="400", description="User not found")
     * )
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        try {
            if (!$request->has('email') || !$request->has('password')) {
                throw new Exception("Missing credentials", 422);
            }
            $credentials = $request->only('email', 'password');
            if (!$token = JWTAuth::attempt($credentials)) {
                throw new Exception("Invalid Credentials", 401);
            }
            $request->request->add(['token' => $token]);
            $cookie = cookie('token', $token, 3600);

            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 400);
            }
            return $this->returnWithSuccess(["token"=> $token, "user" => $user], "User connected")->cookie($cookie)->header('Authorization', $token);
        } catch (Exception $e) {
            return $this->returnWithError($e);
        }
    }

    /**
     * @OA\Get(
     *     path="/api/logout",
     *     operationId="Logout",
     *     tags={"Auth"},
     *     @OA\Response(response="200", description="Logout")
     * )
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        try {
            JWTAuth::invalidate(JWTAuth::getToken());
            Cookie::forget('token');
            return $this->returnWithSuccess('Logged out successfully', 'Logged out successfully')->withCookie(Cookie::forget('token'));;
        } catch (\Exception $ex) {
            return $this->returnWithError($ex);
        }
    }

//    public function currentUser()
//    {
//        try {
//            if (!$user = JWTAuth::parseToken()->authenticate()) {
//                return response()->json(['user_not_found'], 400);
//            }
//            return $this->returnWithSuccess($user);
//        } catch (TokenExpiredException $e) {
//            return $this->returnWithError($e, ['msg' => 'token_expired', 'code' => $e->getCode()]);
//        } catch (TokenInvalidException $e) {
//            return $this->returnWithError($e, ['msg' => 'token_invalid', 'code' => $e->getCode()]);
//        } catch (JWTException $e) {
//            return $this->returnWithError($e, ['msg' => 'token_absent', 'code' => $e->getCode()]);
//        } catch (\Exception $ex) {
//            return $this->returnWithError($ex);
//        }
//    }
//
//    public function forgotPassword(Request $request)
//    {
//        $request->validate([
//            'email' => 'required|email',
//        ]);
//
//        $user = DB::table('users')->where('email', $request->email)
//            ->first();
//
//        if (!$user) {
//            return response()->json(['result' => 'Incorrect information', 'status' => 401]);
//        }
//
//        $token = Str::random(64);
//
//        DB::table('password_resets')->insert([
//            'email' => $request->email,
//            'token' => $token,
//            'created_at' => Carbon::now()
//        ]);
//
//        Mail::send('mails.forgetPassword', ['token' => $token, 'url'=> env('RESET_PASSWORD_URL')], function ($message) use ($request) {
//            $message->to($request->email);
//            $message->subject('Reset Password');
//        });
//        return response()->json(['result' => 'A password reset link has been sent to your mail', 'status' => 200]);
//    }
//
//    public function submitResetPasswordForm(Request $request)
//    {
//        try {
//            $request->validate([
//                'email' => 'required|email',
//                'password' => 'required|confirmed|string|min:5',
//            ]);
//
//            $updatePassword = DB::table('password_reset_tokens')
//                ->where([
//                    'email' => $request->email,
//                    'token' => $request->token
//                ])
//                ->first();
//
//            if (!$updatePassword) {
//                return response()->json(['result' => 'Incorrect information', 'status' => 400]);
//            }
//
//            $user = User::where('email', $request->email)->first();
//            $user->password = Hash::make($request->get('password'));
//            $user->save();
//
//            DB::table('password_reset_tokens')->where(['email' => $request->email])->delete();
//
//            return $this->returnWithSuccess('Password successfully changed');
//        } catch (\Exception $ex) {
//            return $this->returnWithError($ex);
//        }
//    }
}
