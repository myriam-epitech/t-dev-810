<?php

namespace App\Http\Controllers;

use App\Models\Api\Hotel;
use App\Models\Api\Hotels;
use Exception;
use GuzzleHttp\Client;
use Helper;
use Illuminate\Http\Request;
use OpenApi\Annotations as OA;

class HotelController extends Controller
{
    /**
     * @OA\Post(
     *     path="/api/location/hotels",
     *     operationId="GetHotelsByLocation",
     *     tags={"Hotel"},
     *
     *     @OA\RequestBody(
     *     required=true,
     *     description="Get a hotel by location",
     *     @OA\JsonContent(
     *     required={"coordinates"},
     *     @OA\Property(property="coordinates", type="object", example={"latitude": 48.856614, "longitude": 2.3522219}),
     *     @OA\Property(property="filters", type="object", example={"ratings": {1, 2, 3}, "amenities": {"AIR_CONDITIONING", "PARKING", "POOL"}}),
     *    )
     *   ),
     *     @OA\Response(response="200", description="Get a hotel by location"),
     *     @OA\Response(response="422", description="No coordinates given"),
     *     @OA\Response(response="500", description="Internal server error")
     * )
     *
     * )
     */
    public function getHotelsByLocation(Request $request)
    {
        try {
            if (!$request->get('coordinates')) {
                throw new Exception("No coordinates given", 422);
            }
            $coordinates = $request->get('coordinates');
            $latitude = $coordinates['latitude'];
            $longitude = $coordinates['longitude'];
            $token = Helper::getCacheToken();
            $options = [
                'headers' => [
                    'Authorization' => 'Bearer ' . $token
                ]
            ];
            $path = config('services.api.amadeus.urlV1') . '/reference-data/locations/hotels/by-geocode?latitude=' . $latitude . '&longitude=' . $longitude;
            $path = Helper::formatUrlWithFilters($path, $request->get('filters'));
            $client = new Client();
            $response = $client->request('GET', $path, $options);
            $res = Helper::formatAmadeusResponse($response);
            $result = Helper::formatData($res->data, Hotels::class);

            return $this->returnWithSuccess($result);
        } catch (\Exception $ex) {
            return $this->returnWithError($ex);
        }
    }

    /**
     * @OA\Post(
     *     path="/api/location/hotelById",
     *     operationId="GetHotelById",
     *     tags={"Hotel"},
     *     @OA\RequestBody(
     *     required=true,
     *     description="Get a hotel by id",
     *     @OA\JsonContent(
     *     required={"hotel_id"},
     *     @OA\Property(property="hotel_id", type="string", example="MCLONGHM"),
     *     @OA\Property(property="filters", type="object", example={"adults": 2, "roomQuantity": 2, "priceRange": "100-1000", "currency": "USD", "checkInDate": "2023-06-01", "checkOutDate": "2023-06-02"}),
     *   )
     * ),
     *     @OA\Response(response="200", description="Get a hotel pricing by id"),
     *     @OA\Response(response="422", description="No hotel id given"),
     *     @OA\Response(response="500", description="Internal server error")
     * )
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getHotelById(Request $request)
    {
        try {
            $hotelId = $request->get('hotel_id');
            if (!$hotelId) {
                throw new Exception("Hotel id not given", 422);
            }

            $token = Helper::getCacheToken();
            $options = [
                'headers' => [
                    'Authorization' => 'Bearer ' . $token
                ]
            ];
            $client = new Client();
            $path = config('services.api.amadeus.urlV3') . '/shopping/hotel-offers?hotelIds=' . $hotelId;
            $path = Helper::formatUrlWithFilters($path, $request->get('filters'));
//            return $path;
            $response = $client->request('GET', $path, $options);
            $res = json_decode($response->getBody()->getContents());

            $result = Helper::formatData($res->data, Hotel::class);

            return $this->returnWithSuccess($result);

        } catch (\Exception $ex) {
            return $this->returnWithError($ex);
        }
    }

}
