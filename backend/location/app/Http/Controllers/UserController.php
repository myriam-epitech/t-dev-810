<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use OpenApi\Annotations as OA;

class UserController extends Controller
{
    /**
     * @OA\Get(
     *     path="/api/users/{id}",
     *     operationId="Get user",
     *     tags={"User"},
     *     @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="User id",
     *     required=true,
     *     @OA\Schema(
     *     type="integer",
     *     format="int64"
     *    )
     *  ),
     *     @OA\Response(
     *     response="200",
     *     description="Get user",
     *     @OA\JsonContent(
     *     @OA\Property(property="id", type="integer", example="1"),
     *     @OA\Property(property="username", type="string", example="username"),
     *     @OA\Property(property="email", type="string", example="email"),
     *     @OA\Property(property="travelplans", type="object", example="travelplans"),
     *     @OA\Property(property="created_at", type="string", example="2021-05-18T15:00:00.000000Z"),
     *     @OA\Property(property="updated_at", type="string", example="2021-05-18T15:00:00.000000Z")
     *  )
     * ),
     *     @OA\Response(response="422", description="Wrong id given"),
     *     @OA\Response(response="404", description="User not found")
     * )
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    function getUser($id)
    {
        try {
            if(!is_numeric($id)){
                throw new Exception("Wrong id given", 422);
            }
            $user = DB::table('users')
                ->where('id', $id)
                ->first();

            $user->travelplans = DB::table('travel_plans')
                ->where('user_id', $user->id)
                ->first();
            return $this->returnWithSuccess($user, "Get user successful");
        } catch (\Exception $ex) {
            return $this->returnWithError($ex);
        }
    }

    /**
     * @OA\Post(
     *     path="/api/users",
     *     operationId="Create user",
     *     tags={"User"},
     *     @OA\RequestBody(
     *     required=true,
     *     description="Create user",
     *     @OA\JsonContent(
     *     required={"username", "email", "password"},
     *     @OA\Property(property="username", type="string", example="username"),
     *     @OA\Property(property="email", type="string", example="email@email.fr"),
     *     @OA\Property(property="password", type="string", example="password"),
     *     @OA\Property(property="password_confirmation", type="string", example="password"),
     *     )
     * ),
     *     @OA\Response(
     *     response="200",
     *     description="Create user",
     *     @OA\JsonContent(
     *     @OA\Property(property="id", type="integer", example="1"),
     *     @OA\Property(property="username", type="string", example="username"),
     *     @OA\Property(property="email", type="string", example="email@email.fr"),
     *     @OA\Property(property="created_at", type="string", example="2021-05-18T15:00:00.000000Z"),
     *     @OA\Property(property="updated_at", type="string", example="2021-05-18T15:00:00.000000Z")
     * )
     * ),
     *     @OA\Response(response="422", description="Wrong id given"),
     *     @OA\Response(response="404", description="User not found")
     * )
     *
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    function create(Request $request)
    {
        try {
            $validateUser = Validator::make($request->all(),
                [
                    'username'=> ['required', 'string', 'max:255'],
                    'email'=> ['required', 'email', 'max:255'],
                    'password'=> ['required', 'string', 'max:255', 'confirmed'],
                ]);

            if ($validateUser->fails()) {
                throw new \Exception($validateUser->errors(), 422);
            }

            $user = User::create([
                'username' => $request->get('username'),
                'email' => $request->get('email'),
                'password' => Hash::make($request->get('password')),
            ]);
            return $this->returnWithSuccess($user);
        } catch (\Exception $ex) {
            return $this->returnWithError($ex);
        }
    }

    /**
     * @OA\Patch(
     *     path="/api/users/{id}",
     *     operationId="Update user",
     *     tags={"User"},
     *     @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="User id",
     *     required=true,
     *     @OA\Schema(
     *     type="integer",
     *     format="int64"
     *   )
     * ),
     *     @OA\RequestBody(
     *     required=true,
     *     description="Update user",
     *     @OA\JsonContent(
     *     required={"username", "email", "password"},
     *     @OA\Property(property="username", type="string", example="username"),
     *     @OA\Property(property="email", type="string", example="email@email.fr"),
     *     @OA\Property(property="password", type="string", example="password"),
     *     @OA\Property(property="password_confirmation", type="string", example="password"),
     *     @OA\Property(property="old_password", type="string", example="password"),
     *     )
     * ),
     *     @OA\Response(
     *     response="200",
     *     description="Update user",
     *     @OA\JsonContent(
     *     @OA\Property(property="id", type="integer", example="1"),
     *     @OA\Property(property="username", type="string", example="username"),
     *     @OA\Property(property="email", type="string", example="email"),
     *     @OA\Property(property="created_at", type="string", example="2021-05-18T15:00:00.000000Z"),
     *     @OA\Property(property="updated_at", type="string", example="2021-05-18T15:00:00.000000Z")
     * )
     * ),
     *     @OA\Response(response="422", description="Wrong id given"),
     *     @OA\Response(response="404", description="User not found")
     * )
     *
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    function update($id, Request $request)
    {
        try {
            $validateUser = Validator::make($request->all(),
                [
                    'username'=> ['string', 'max:255'],
                    'email'=> ['email', 'max:255'],
                    'password'=> ['string', 'max:255', 'confirmed']
                ]);

            if ($validateUser->fails()) {
                throw new \Exception($validateUser->errors(), 422);
            }

            $user = User::findOrFail($id);
            if ($request->get('username')) {
                $user->username = $request->get('username');
            }
            if ($request->get('email')) {
                $user->email = $request->get('email');
            }
            if ($request->get('password')) {
                if (Hash::check($request->get('old_password'), $user->password)) {
                    $user->password = Hash::make($request->get('password'));
                } else {
                    return response()->json(['result' => 'Wrong password confirmation', 'status' => 0]);
                }
            }
            $user->saveOrFail();
            return $this->returnWithSuccess($user);
        } catch (\Exception $ex) {
            return $this->returnWithError($ex);
        }
    }

    /**
     * @OA\Delete(
     *     path="/api/users/{id}",
     *     operationId="Delete user",
     *     tags={"User"},
     *     @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="User id",
     *     required=true,
     *     @OA\Schema(
     *     type="integer",
     *     format="int64"
     *  )
     * ),
     *     @OA\Response(
     *     response="200",
     *     description="Delete user",
     *     @OA\JsonContent(
     *     @OA\Property(property="id", type="integer", example="1"),
     *     @OA\Property(property="username", type="string", example="username"),
     *     @OA\Property(property="email", type="string", example="email"),
     *     @OA\Property(property="created_at", type="string", example="2021-05-18T15:00:00.000000Z"),
     *     @OA\Property(property="updated_at", type="string", example="2021-05-18T15:00:00.000000Z")
     * )
     * ),
     *     @OA\Response(response="422", description="Wrong id given"),
     *     @OA\Response(response="404", description="User not found")
     * )
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    function delete($id)
    {
        try {
            if(!is_numeric($id)){
                throw new Exception("Wrong id given", 422);
            }
            $user = User::findOrFail($id);
            $user->delete();
            return $this->returnWithSuccess($user);
        } catch (\Exception $ex) {
            return $this->returnWithError($ex);
        }
    }
}
