<?php

namespace App\Http\Controllers;

use App\Models\Api\Location;
use App\Models\Api\Pois;
use Exception;
use GuzzleHttp\Client;
use Helper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Http;
use OpenApi\Annotations as OA;

class LocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @OA\Post(
     *     path="/api/location",
     *     operationId="GetCoordinateByString",
     *     tags={"Location"},
     *
     *     @OA\RequestBody(
     *     required=true,
     *     description="Get a location by string",
     *     @OA\JsonContent(
     *     required={"location"},
     *     @OA\Property(property="location", type="string", example="Paris")
     *    )
     *   ),
     *     @OA\Response(response="200", description="Get a location by string"),
     *     @OA\Response(response="422", description="Location not given")
     * )
     *
     * )
     */
    public function getCoordinateByString(Request $request)
    {
        try {
            if (!$request->get('location')) {
                throw new Exception("Location not given", 422);
            }
            $location = $request->get('location');
            $path = config('services.api.mapbox.apikey') . $location . ".json?access_token=" . config('services.api.mapbox.pubkey');
            $response = Http::get($path);

            $result = Helper::formatData($response->object()->features, Location::class);

            return $this->returnWithSuccess($result);
        } catch (\Exception $ex) {
            return $this->returnWithError($ex);
        }
    }

    /**
     * @OA\Post(
     *     path="/api/location/pois",
     *     operationId="GetPoisByCoordinates",
     *     tags={"Location"},
     *     @OA\RequestBody(
     *     required=true,
     *     description="Get point of interest by coordinates",
     *     @OA\JsonContent(
     *     required={"coordinates"},
     *     @OA\Property(property="coordinates", type="object", example={"latitude": 48.856614, "longitude": 2.3522219}),
     *     @OA\Property(property="radius", type="integer", example=10)
     *   )
     * ),
     *     @OA\Response(response="200", description="Get point of interest by coordinates"),
     *     @OA\Response(response="422", description="No coordinates given")
     * )
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getPois(Request $request) {
        try {
            $token = Helper::getCacheToken();
            $coordinates = $request->get('coordinates');
            $radius = $request->get('radius');
            if (!$radius) {
                $radius = 10;
            }
            if (!$coordinates) {
                throw new Exception("No coordinates given", 422);
            }
            $options = [
                'headers' => [
                    'Authorization' => 'Bearer ' . $token
                ]
            ];
            $latitude = $coordinates['latitude'];
            $longitude = $coordinates['longitude'];
            if (Helper::checkCacheLocationChanges($coordinates)) {
                $path = config('services.api.amadeus.urlV1') . '/reference-data/locations/pois?latitude='. $latitude . '&longitude=' . $longitude . '&radius=' . $radius;
                $next = $request->get('next');
                if ($next) {
                    $path = $next;
                }
                $client = new Client();
                $response = $client->request('GET', $path, $options);
                $res = Helper::formatAmadeusResponse($response);
                $result = Helper::formatData($res->data, Pois::class);
                $result = ["pois" => $result, "meta" => $res->meta];
                Helper::setCachePois($result);
            } else {
                $result = Helper::getCachePois();
            }

            return $this->returnWithSuccess($result);

        } catch (\Exception $ex) {
            return $this->returnWithError($ex);
        }
    }
}
