<h1>Reset Password</h1>

Click on the following link to reset your password:
<a href="{{ $url }}?treset={{ $token }}">Reset Password</a>
