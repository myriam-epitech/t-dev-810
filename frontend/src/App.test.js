import React from 'react';

import { render, screen } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom';

import App from './App';


describe('App', () => {
  test('renders home page', () => {
    render(
      <MemoryRouter initialEntries={['/']}>
        <App />
      </MemoryRouter>
    );
    const page = screen.getByTestId('home-page');
    expect(page).toBeInTheDocument();
  });

  test('renders fly page', () => {
    render(
      <MemoryRouter initialEntries={['/home']}>
        <App />
      </MemoryRouter>
    );
    const page = screen.getByTestId('home-page');
    expect(page).toBeInTheDocument();
  });
  test('renders pois page', () => {
    render(
      <MemoryRouter initialEntries={['/pois']}>
        <App />
      </MemoryRouter>
    );
    const page = screen.getByTestId('pois-page');
    expect(page).toBeInTheDocument();
  });
  test('renders hotel page', () => {
    render(
      <MemoryRouter initialEntries={['/hotel']}>
        <App />
      </MemoryRouter>
    );
    const page = screen.getByTestId('hotel-page');
    expect(page).toBeInTheDocument();
  });
  test('renders transports page', () => {
    render(
      <MemoryRouter initialEntries={['/transports']}>
        <App />
      </MemoryRouter>
    );
    const page = screen.getByTestId('transports-page');
    expect(page).toBeInTheDocument();
  });
  test('renders resume page', () => {
    render(
      <MemoryRouter initialEntries={['/resume']}>
        <App />
      </MemoryRouter>
    );
    const page = screen.getByTestId('resume-page');
    expect(page).toBeInTheDocument();
  });
});
