import { open } from "./open";

describe("open", () => {
    test("Test if open is true", () => {
        expect(open(true).width).toBe("200px")
    }),
    test("Test if open is false", () => {
        expect(open(false).width).toBe("70px")
    })
})
