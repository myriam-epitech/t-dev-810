export function open(isOpen) {
    return {
        width: isOpen ? "200px" : "70px",

        transition: {
          duration: 0.5,
          type: "spring",
          damping: 15,
        }
    }
};
