export const links = [
  { name: "Home", to: "/home", id: 1, fa: "fa fa-home" },
  { name: "A voir", to: "/pois", id: 2, fa: "fa fa-church" },
  { name: "Hotels", to: "/hotel", id: 3, fa: "fa fa-hotel" },
  { name: "Transports", to: "/transports", id: 4, fa: "fa fa-train" },
];
