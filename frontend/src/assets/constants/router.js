export const ROUTES = {
    host: "http://localhost:3000",
    login: "/login",
    register: "/register",
    home: "/home",
    profile: "/profile",
    my_travels: "/my-travels",
    my_travel: "/my-travel",
};
  