import React, { useEffect, useState } from "react";
import Tourist from "../../assets/images/tourist_map.svg";
import SectionResult from "../../component/Results/SectionResults";
import { post } from "../../helpers/axiosHelper";
import {data, formatDataPOIS} from "./data";
import { resultType } from "../../assets/constants/type";

function Pois() {
  const initialLatLng = [48.8, 2.3];
  const [locationData, setLocationData] = useState({});

  useEffect(() => {
    if (localStorage.getItem("Place") === null) return;

    const latLng = localStorage.getItem("Place").split(",");

    post("location/pois", {
      // coordinates: { latitude: latLng[0], longitude: latLng[1] },
      coordinates: { latitude: initialLatLng[0], longitude: initialLatLng[1] },
    }).then((resp) => {
      //Since the API does'nt work properly, i extracted a json data. The code should be below
      // setLocationData(locationData.data);
    
    });

    setLocationData(formatDataPOIS(data.data.pois))
  });

  return (
    <div className="container">
      <section className="header">
        <img className="background-image" src={Tourist} />
        <h1 data-testid="pois-page">A voir</h1>
      </section>

      <SectionResult cardType={resultType.POIS} locationData={locationData} latLng={initialLatLng} />
    </div>
  );
}

export default Pois;
