import React from "react"
import { data, formatDataPOIS } from "./data"

var mockData = {
    data: {
      pois: [
        {
          name: "Cathédrale Notre-Dame de Paris",
          category: "SIGHTS",
          tags: [
            "church",
            "sightseeing",
            "restaurant",
            "tourguide",
            "sights",
            "temple",
            "landmark",
            "historicplace",
            "historic",
            "attraction",
            "activities",
            "professionalservices",
            "sports",
            "bike",
            "rental",
            "commercialplace",
            "outdoorplace",
          ],
          coordinates: {
            latitude: 48.852966,
            longitude: 2.349902,
          },
        },
        {
          name: "Jardin du Luxembourg",
          category: "SIGHTS",
          tags: [
            "sightseeing",
            "restaurant",
            "tourguide",
            "sights",
            "park",
            "landmark",
            "historicplace",
            "historic",
            "garden",
            "attraction",
            "activities",
            "hiking",
            "beauty&spas",
            "theater",
            "commercialplace",
            "nature",
          ],
          coordinates: {
            latitude: 48.84681,
            longitude: 2.337546,
          },
        },
        {
          name: "Centre Pompidou",
          category: "SIGHTS",
          tags: [
            "museum",
            "sightseeing",
            "restaurant",
            "artgallerie",
            "tourguide",
            "sights",
            "transport",
            "activities",
            "attraction",
            "shopping",
            "square",
            "parking",
            "professionalservices",
            "bus",
            "theater",
            "events",
            "commercialplace",
          ],
          coordinates: {
            latitude: 48.860825,
            longitude: 2.352633,
          },
        },
        {
          name: "Jardin des Tuileries",
          category: "SIGHTS",
          tags: [
            "restaurant",
            "landmark",
            "sightseeing",
            "garden",
            "park",
            "tourguide",
            "sights",
            "attraction",
            "nature",
            "commercialplace",
            "professionalservices",
            "hiking",
            "beauty&spas",
            "events",
            "activities",
          ],
          coordinates: {
            latitude: 48.864246,
            longitude: 2.324892,
          },
        },
        {
          name: "Opéra National de Paris - Palais Garnier",
          category: "RESTAURANT",
          tags: [
            "restaurant",
            "events",
            "attraction",
            "activities",
            "operahouse",
            "sightseeing",
            "commercialplace",
            "theater",
            "sights",
            "schools",
            "tourguide",
            "landmark",
            "musicvenue",
            "hiking",
            "bar",
            "publicplace",
          ],
          coordinates: {
            latitude: 48.871944,
            longitude: 2.331666,
          },
        },
        {
          name: "Angelina",
          category: "RESTAURANT",
          tags: [
            "restaurant",
            "tea",
            "sightseeing",
            "tourguide",
            "shopping",
            "attraction",
            "activities",
            "bakery",
            "commercialplace",
            "transport",
            "health&medical",
          ],
          coordinates: {
            latitude: 48.865093,
            longitude: 2.328464,
          },
        },
        {
          name: "Galeries Lafayette Haussmann",
          category: "RESTAURANT",
          tags: [
            "restaurant",
            "sightseeing",
            "shopping",
            "tourguide",
            "mall",
            "clothing",
            "fashion",
            "attraction",
            "activities",
            "parking",
            "professionalservices",
            "taxi",
            "liquor",
          ],
          coordinates: {
            latitude: 48.87301,
            longitude: 2.33269,
          },
        },
        {
          name: "Café de la Paix",
          category: "RESTAURANT",
          tags: ["restaurant", "sightseeing", "shopping", "commercialplace"],
          coordinates: {
            latitude: 48.870876,
            longitude: 2.331753,
          },
        },
        {
          name: "Le Grand Véfour",
          category: "RESTAURANT",
          tags: [
            "restaurant",
            "sightseeing",
            "commercialplace",
            "professionalservices",
            "events",
            "activities",
          ],
          coordinates: {
            latitude: 48.86621,
            longitude: 2.33801,
          },
        },
        {
          name: "Île de la Cité",
          category: "RESTAURANT",
          tags: [
            "restaurant",
            "tourguide",
            "attraction",
            "professionalservices",
            "landmark",
            "activities",
          ],
          coordinates: {
            latitude: 48.85677,
            longitude: 2.345903,
          },
        },
      ],
      meta: {
        count: 1339,
        links: {
          self: "https://test.api.amadeus.com/v1/reference-data/locations/pois?latitude=48.8&longitude=2.3&radius=10",
          next: "https://test.api.amadeus.com/v1/reference-data/locations/pois?latitude=48.8&longitude=2.3&radius=10&page[offset]=10&page[limit]=10",
          last: "https://test.api.amadeus.com/v1/reference-data/locations/pois?latitude=48.8&longitude=2.3&radius=10&page[offset]=1330&page[limit]=10",
          first:
            "https://test.api.amadeus.com/v1/reference-data/locations/pois?latitude=48.8&longitude=2.3&radius=10&page[offset]=0&page[limit]=10",
          up: "https://test.api.amadeus.com/v1/reference-data/locations/pois?latitude=48.8&longitude=2.3&radius=10",
        },
      },
    },
    message: "",
    status: 1,
  };

describe("Data for hotel testing", () => {
    test("If json file is beeing retrieved correctly", ()=> {
        expect(data).toEqual(mockData)
    })

    test("Length of array return to be equal to 10", () => {
        expect(formatDataPOIS(mockData.data.pois).length).toBe(10)
        
        var dataFormated = formatDataPOIS(mockData.data.pois)
        expect(dataFormated[1].buttonText).toStrictEqual("Ajouter au trajet")
    })
})
