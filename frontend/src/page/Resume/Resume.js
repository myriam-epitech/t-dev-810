import React, { useEffect, useState } from "react";
import tourist_map from "../../assets/images/tourist_map.svg";
import LocationCards from "../../component/LocationCards/LocationCards";
import ReactToPdf from "react-to-pdf"
const ref = React.createRef();

function Resume() {
  const [tripItem, setTripItem] = useState({});
  const options = {
    orientation: 'landscape',
    unit: 'in',
    format: [20,10]
};
  useEffect(() => {
    if (localStorage.getItem("tripItems") === null) return;
    const data = JSON.parse(localStorage.getItem("tripItems"));
    var newFormatedData = [];
    Object.keys(data).map((key) => {
      newFormatedData.push(...data[key]);
    });
    setTripItem(newFormatedData);
  }, []);

  useEffect(() => {}, [tripItem]);
  return (
    <div className="container">
      <section className="header">
        <img className="background-image" src={tourist_map} />
        <h1 data-testid="resume-page">Resume</h1>
        <ReactToPdf targetRef={ref} filename="div-blue.pdf" options={options} x={1} y={1} scale={0.8}>
          {({ toPdf }) => <button onClick={toPdf}>Generate pdf whouhou</button>}
        </ReactToPdf>
      </section>


      
      <div ref={ref}>
        <LocationCards
          cardType={"resume"}
          data={tripItem}
          changeMapLocation={() => {}}
        />
      </div>
    </div>
  );
}

export default Resume;
