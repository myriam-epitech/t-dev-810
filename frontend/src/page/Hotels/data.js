const data = {
  data: [
    {
      id: "BWORY624",
      name: "BEST WESTERN PARC DES EXPOS",
      latitude: 48.81661,
      longitude: 2.30638,
      distance: {
        value: 1.9,
        unit: "KM"
      }
    },
    {
      id: "NNPARC24",
      name: "CAMPANILE PARIS SUD - PORTE D'ORLEANS",
      latitude: 48.80562,
      longitude: 2.32551,
      distance: {
        value: 1.97,
        unit: "KM"
      }
    },
    {
      id: "RTPARPCH",
      name: "ADAGIO ACCESS VANVES CHATILLON",
      latitude: 48.81769,
      longitude: 2.2873,
      distance: {
        value: 2.17,
        unit: "KM"
      }
    },
    {
      id: "FGPARSHP",
      name: "SUITE HOME PORTE DE CHATILLON",
      latitude: 48.81972,
      longitude: 2.31042,
      distance: {
        value: 2.32,
        unit: "KM"
      }
    },
    {
      id: "CIPAR488",
      name: "COMFORT HOTEL PARIS SUD MONTROUGE",
      latitude: 48.81785,
      longitude: 2.31707,
      distance: {
        value: 2.34,
        unit: "KM"
      }
    },
    {
      id: "RTPARVNN",
      name: "IBIS PARIS VANVES PARC EXPO",
      latitude: 48.82214,
      longitude: 2.29429,
      distance: {
        value: 2.49,
        unit: "KM"
      }
    },
    {
      id: "HSPARBUS",
      name: "PARC EVEN",
      latitude: 48.82246,
      longitude: 2.3033,
      distance: {
        value: 2.51,
        unit: "KM"
      }
    },
    {
      id: "HSPARBVC",
      name: "PATIO BRANCION",
      latitude: 48.82287,
      longitude: 2.30002,
      distance: {
        value: 2.54,
        unit: "KM"
      }
    },
    {
      id: "HSPARBQP",
      name: "TROSY",
      latitude: 48.80277,
      longitude: 2.2628,
      distance: {
        value: 2.74,
        unit: "KM"
      }
    },
    {
      id: "RTPARIBS",
      name: "IBIS STYLES PARIS PTE ORLEANS",
      latitude: 48.81886,
      longitude: 2.32568,
      distance: {
        value: 2.81,
        unit: "KM"
      }
    },
    {
      id: "HSPARBSV",
      name: "LA BRECHE DU BOIS CITOTEL",
      latitude: 48.79983,
      longitude: 2.26059,
      distance: {
        value: 2.88,
        unit: "KM"
      }
    },
    {
      id: "QIPAR366",
      name: "QUALITY HOTEL PARIS ORLEANS",
      latitude: 48.82346,
      longitude: 2.32401,
      distance: {
        value: 3.14,
        unit: "KM"
      }
    },
    {
      id: "RTORYISS",
      name: "IBIS PARIS PORTE DE VERSAILLES",
      latitude: 48.82315,
      longitude: 2.2749,
      distance: {
        value: 3.16,
        unit: "KM"
      }
    },
    {
      id: "HSORYAET",
      name: "IDEAL HOTEL DESIGN",
      latitude: 48.8231,
      longitude: 2.32606,
      distance: {
        value: 3.2,
        unit: "KM"
      }
    },
    {
      id: "ACPAR499",
      name: "CLARISSE",
      latitude: 48.82878,
      longitude: 2.30069,
      distance: {
        value: 3.2,
        unit: "KM"
      }
    },
    {
      id: "FGPARSTA",
      name: "STARS ARCUEIL",
      latitude: 48.79863,
      longitude: 2.34414,
      distance: {
        value: 3.24,
        unit: "KM"
      }
    },
    {
      id: "CHPAR641",
      name: "CLASSICS PARC DES EXPOSITIONS",
      latitude: 48.82326,
      longitude: 2.27312,
      distance: {
        value: 3.25,
        unit: "KM"
      }
    },
    {
      id: "AZPAR308",
      name: "CITADINES DIDOT MONTPARNASSE",
      latitude: 48.8278,
      longitude: 2.31517,
      distance: {
        value: 3.28,
        unit: "KM"
      }
    },
    {
      id: "ACPAR900",
      name: "CHATILLON PARIS MONTPARNASSE",
      latitude: 48.82576,
      longitude: 2.32323,
      distance: {
        value: 3.33,
        unit: "KM"
      }
    },
    {
      id: "NNPARC22",
      name: "CAMPANILE VILLEJUIF",
      latitude: 48.79683,
      longitude: 2.34613,
      distance: {
        value: 3.4,
        unit: "KM"
      }
    },
    {
      id: "CHPAR642",
      name: "CLASSICS PORTE DE VERSAILLES",
      latitude: 48.8287,
      longitude: 2.28287,
      distance: {
        value: 3.43,
        unit: "KM"
      }
    },
    {
      id: "HSPARBVB",
      name: "LUXOR",
      latitude: 48.82886,
      longitude: 2.28232,
      distance: {
        value: 3.46,
        unit: "KM"
      }
    },
    {
      id: "RTPAR666",
      name: "ADAGIO PORTE DE VERSAILLES",
      latitude: 48.83145,
      longitude: 2.28144,
      distance: {
        value: 3.75,
        unit: "KM"
      }
    },
    {
      id: "RTPARVAU",
      name: "MERCURE  PARIS VAUGIRARD PTE  V",
      latitude: 48.8329,
      longitude: 2.28808,
      distance: {
        value: 3.76,
        unit: "KM"
      }
    },
    {
      id: "RTPARALI",
      name: "IBIS STYLES PARIS ALESIA",
      latitude: 48.83017,
      longitude: 2.32344,
      distance: {
        value: 3.77,
        unit: "KM"
      }
    },
    {
      id: "ACPARA47",
      name: "TERMINUS VAUGIRARD",
      latitude: 48.83313,
      longitude: 2.28907,
      distance: {
        value: 3.77,
        unit: "KM"
      }
    },
    {
      id: "ACPARD16",
      name: "FRED HOTEL",
      latitude: 48.83221,
      longitude: 2.31637,
      distance: {
        value: 3.78,
        unit: "KM"
      }
    },
    {
      id: "HAPARMIN",
      name: "ARIANE MONTPARNASSE HOTEL",
      latitude: 48.83211,
      longitude: 2.32177,
      distance: {
        value: 3.91,
        unit: "KM"
      }
    },
    {
      id: "ACPAR245",
      name: "ATELIER MONTPARNASSE",
      latitude: 48.83336,
      longitude: 2.31691,
      distance: {
        value: 3.91,
        unit: "KM"
      }
    },
    {
      id: "ACPAR669",
      name: "AJIEL",
      latitude: 48.83553,
      longitude: 2.30234,
      distance: {
        value: 3.95,
        unit: "KM"
      }
    },
    {
      id: "PUPARSOF",
      name: "PULLMAN PARIS RIVE GAUCHE",
      latitude: 48.83295,
      longitude: 2.27786,
      distance: {
        value: 4,
        unit: "KM"
      }
    },
    {
      id: "ACPAR445",
      name: "AVENIR",
      latitude: 48.83633,
      longitude: 2.2947,
      distance: {
        value: 4.06,
        unit: "KM"
      }
    },
    {
      id: "QIPAR197",
      name: "QUALITY HOTEL ABACA MESSIDOR - PARIS 15",
      latitude: 48.83672,
      longitude: 2.29542,
      distance: {
        value: 4.09,
        unit: "KM"
      }
    },
    {
      id: "ACPARE71",
      name: "SOPHIE GERMAIN",
      latitude: 48.83099,
      longitude: 2.33038,
      distance: {
        value: 4.1,
        unit: "KM"
      }
    },
    {
      id: "RTPARMMP",
      name: "IBIS PARIS MAINE MONTPARNASSE",
      latitude: 48.83371,
      longitude: 2.32289,
      distance: {
        value: 4.11,
        unit: "KM"
      }
    },
    {
      id: "FGPARRPM",
      name: "RESIDHOME PARIS ISSY LES MOULINEAUX",
      latitude: 48.82609,
      longitude: 2.26029,
      distance: {
        value: 4.11,
        unit: "KM"
      }
    },
    {
      id: "NNPARC13",
      name: "CAMPANILE PARIS 14 - MAINE MONTPARNASSE",
      latitude: 48.8344,
      longitude: 2.32398,
      distance: {
        value: 4.21,
        unit: "KM"
      }
    },
    {
      id: "BLPARR88",
      name: "EXPEDIA CERTIFICATION PROPERTY JAN 2013(",
      latitude: 48.7825,
      longitude: 2.35307,
      distance: {
        value: 4.35,
        unit: "KM"
      }
    },
    {
      id: "NNPARC06",
      name: "CAMPANILE - PARIS (PORTE DITALIE)",
      latitude: 48.81022,
      longitude: 2.35732,
      distance: {
        value: 4.35,
        unit: "KM"
      }
    },
    {
      id: "RTPARALL",
      name: "IBIS STYLES PARIS 15 LECOURBE",
      latitude: 48.83871,
      longitude: 2.28945,
      distance: {
        value: 4.37,
        unit: "KM"
      }
    },
    {
      id: "EXPAR05M",
      name: "HOTEL DE LA PAIX",
      latitude: 48.83866,
      longitude: 2.28867,
      distance: {
        value: 4.38,
        unit: "KM"
      }
    },
    {
      id: "FGPARQUI",
      name: "HOTEL RESIDENCE QUINTINIE",
      latitude: 48.83916,
      longitude: 2.30656,
      distance: {
        value: 4.38,
        unit: "KM"
      }
    },
    {
      id: "CDPARBLE",
      name: "CONCORDE MONTPARNASSE - PARIS",
      latitude: 48.83748,
      longitude: 2.31865,
      distance: {
        value: 4.38,
        unit: "KM"
      }
    },
    {
      id: "RTPARGMP",
      name: "NOVOTEL PARIS MONTPARNASSE",
      latitude: 48.83845,
      longitude: 2.3152,
      distance: {
        value: 4.42,
        unit: "KM"
      }
    },
    {
      id: "RTPARVGR",
      name: "NOVOTEL PARIS VAUGIRARD MONTP",
      latitude: 48.83997,
      longitude: 2.30303,
      distance: {
        value: 4.45,
        unit: "KM"
      }
    },
    {
      id: "ACPARF44",
      name: "MISTRAL",
      latitude: 48.83688,
      longitude: 2.32369,
      distance: {
        value: 4.45,
        unit: "KM"
      }
    },
    {
      id: "HIPAR2FB",
      name: "HOLIDAY INN GARE MONTPARNASSE",
      latitude: 48.83826,
      longitude: 2.32287,
      distance: {
        value: 4.57,
        unit: "KM"
      }
    },
    {
      id: "MCPARSTM",
      name: "PARIS MARRIOTT RIVE GAUCHE",
      latitude: 48.83188,
      longitude: 2.33953,
      distance: {
        value: 4.58,
        unit: "KM"
      }
    },
    {
      id: "HSPARCDF",
      name: "LA FERME DES BARMONTS",
      latitude: 48.80209,
      longitude: 2.36281,
      distance: {
        value: 4.61,
        unit: "KM"
      }
    },
    {
      id: "BWPAR126",
      name: "BEST WESTERN MONTCALM",
      latitude: 48.84081,
      longitude: 2.28722,
      distance: {
        value: 4.63,
        unit: "KM"
      }
    },
    {
      id: "HSPARART",
      name: "SAINT CHARLES",
      latitude: 48.82714,
      longitude: 2.34832,
      distance: {
        value: 4.65,
        unit: "KM"
      }
    },
    {
      id: "NNPARC49",
      name: "KYRIAD FRESNES",
      latitude: 48.75988,
      longitude: 2.32059,
      distance: {
        value: 4.71,
        unit: "KM"
      }
    },
    {
      id: "ACPAR971",
      name: "HOTEL INNOVA",
      latitude: 48.84219,
      longitude: 2.31292,
      distance: {
        value: 4.79,
        unit: "KM"
      }
    },
    {
      id: "RTPARTOL",
      name: "IBIS PARIS ITALIE TOLBIAC 13E",
      latitude: 48.82582,
      longitude: 2.35259,
      distance: {
        value: 4.8,
        unit: "KM"
      }
    },
    {
      id: "ACPARNOU",
      name: "NOUVEL EIFFEL",
      latitude: 48.84335,
      longitude: 2.30675,
      distance: {
        value: 4.85,
        unit: "KM"
      }
    },
    {
      id: "RTPARCLA",
      name: "ADAGIO ACCESS PARIS CLAMART",
      latitude: 48.78083,
      longitude: 2.24033,
      distance: {
        value: 4.86,
        unit: "KM"
      }
    },
    {
      id: "HSORYAFE",
      name: "AVIA SAPHIR MONTPARNASSE",
      latitude: 48.84304,
      longitude: 2.31317,
      distance: {
        value: 4.88,
        unit: "KM"
      }
    },
    {
      id: "HSPARAIH",
      name: "ISTRIA",
      latitude: 48.83909,
      longitude: 2.33158,
      distance: {
        value: 4.92,
        unit: "KM"
      }
    },
    {
      id: "OIPARAIG",
      name: "HOTEL AIGLON",
      latitude: 48.83964,
      longitude: 2.33045,
      distance: {
        value: 4.94,
        unit: "KM"
      }
    },
    {
      id: "OIPAR5IH",
      name: "HOTEL LE CHATEAUBRIAND - CHATENAY-MALABR",
      latitude: 48.76736,
      longitude: 2.25396,
      distance: {
        value: 4.95,
        unit: "KM"
      }
    },
    {
      id: "ACPARA11",
      name: "DELAMBRE MONTPARNASSE",
      latitude: 48.84135,
      longitude: 2.32604,
      distance: {
        value: 4.98,
        unit: "KM"
      }
    },
    {
      id: "HSPARBHQ",
      name: "SUITES UNIC SAINT GERMAIN",
      latitude: 48.8418,
      longitude: 2.3254,
      distance: {
        value: 5.01,
        unit: "KM"
      }
    },
    {
      id: "HSORYABF",
      name: "ALYSS SAPHIR CAMBRONNE EIFFEL",
      latitude: 48.84502,
      longitude: 2.29741,
      distance: {
        value: 5.01,
        unit: "KM"
      }
    },
    {
      id: "RTPARMAI",
      name: "IBIS PARIS TOUR MONTPARNASSE",
      latitude: 48.84333,
      longitude: 2.32019,
      distance: {
        value: 5.04,
        unit: "KM"
      }
    },
    {
      id: "RTPARKRE",
      name: "NOVOTEL PARIS PORTE D'ITALIE",
      latitude: 48.81606,
      longitude: 2.36437,
      distance: {
        value: 5.04,
        unit: "KM"
      }
    },
    {
      id: "BWPAR620",
      name: "BEST WESTERN AMIRAL HOTEL",
      latitude: 48.82533,
      longitude: 2.35752,
      distance: {
        value: 5.07,
        unit: "KM"
      }
    },
    {
      id: "UIPARLNX",
      name: "LENOX MONTPARNASSE HOTEL",
      latitude: 48.84173,
      longitude: 2.32805,
      distance: {
        value: 5.07,
        unit: "KM"
      }
    },
    {
      id: "OIPARVIC",
      name: "HOTEL VIC EIFFEL",
      latitude: 48.84536,
      longitude: 2.31012,
      distance: {
        value: 5.1,
        unit: "KM"
      }
    },
    {
      id: "RTPARBLA",
      name: "MERCURE PARIS PLACE ITALIE 3*",
      latitude: 48.8303,
      longitude: 2.35283,
      distance: {
        value: 5.13,
        unit: "KM"
      }
    },
    {
      id: "ACPAR410",
      name: "DES ACADEMIES ET DES ARTS",
      latitude: 48.8421,
      longitude: 2.33041,
      distance: {
        value: 5.18,
        unit: "KM"
      }
    },
    {
      id: "HSPARBOC",
      name: "BALDI",
      latitude: 48.84658,
      longitude: 2.30552,
      distance: {
        value: 5.2,
        unit: "KM"
      }
    },
    {
      id: "ACPAR784",
      name: "APOSTROPHE RIVE GAUCHE",
      latitude: 48.84181,
      longitude: 2.33178,
      distance: {
        value: 5.2,
        unit: "KM"
      }
    },
    {
      id: "BWPAR729",
      name: "BEST WESTERN HOTEL LE MONTPARNASSE",
      latitude: 48.84476,
      longitude: 2.32107,
      distance: {
        value: 5.21,
        unit: "KM"
      }
    },
    {
      id: "FGPARVOI",
      name: "HOTEL BEAUVOIR",
      latitude: 48.83989,
      longitude: 2.33738,
      distance: {
        value: 5.21,
        unit: "KM"
      }
    },
    {
      id: "XKPAR880",
      name: "HOLIDAY INN AUTEUIL",
      latitude: 48.83807,
      longitude: 2.25826,
      distance: {
        value: 5.22,
        unit: "KM"
      }
    },
    {
      id: "UIPAR614",
      name: "HOTEL EIFFEL SEGUR",
      latitude: 48.84691,
      longitude: 2.3046,
      distance: {
        value: 5.23,
        unit: "KM"
      }
    },
    {
      id: "BWPAR295",
      name: "BEST WESTERN VILLA DES ARTISTES",
      latitude: 48.84238,
      longitude: 2.33102,
      distance: {
        value: 5.23,
        unit: "KM"
      }
    },
    {
      id: "ACPARF06",
      name: "BOULOGNE RESIDENCE HOTEL",
      latitude: 48.83152,
      longitude: 2.24691,
      distance: {
        value: 5.23,
        unit: "KM"
      }
    },
    {
      id: "EUPAR776",
      name: "EXCLUSIVE LE BREA LUXEMBOURG",
      latitude: 48.84289,
      longitude: 2.32982,
      distance: {
        value: 5.25,
        unit: "KM"
      }
    },
    {
      id: "ACPARCHA",
      name: "LE CHAPLAIN PARIS RIVE GAUCHE",
      latitude: 48.84292,
      longitude: 2.33036,
      distance: {
        value: 5.26,
        unit: "KM"
      }
    },
    {
      id: "UIPAR427",
      name: "LE SIX SAINT GERMAIN HOTEL",
      latitude: 48.84365,
      longitude: 2.32805,
      distance: {
        value: 5.27,
        unit: "KM"
      }
    },
    {
      id: "CHPAR041",
      name: "HOTEL ALIZE GRENELLE TOUR EIFFEL",
      latitude: 48.84674,
      longitude: 2.28777,
      distance: {
        value: 5.27,
        unit: "KM"
      }
    },
    {
      id: "ACPARD37",
      name: "FIRST HOTEL PARIS TOUR EIFFEL",
      latitude: 48.84751,
      longitude: 2.30256,
      distance: {
        value: 5.29,
        unit: "KM"
      }
    },
    {
      id: "RTPARGOB",
      name: "MERCURE PARIS GOBELINS ITALIE",
      latitude: 48.83202,
      longitude: 2.35356,
      distance: {
        value: 5.3,
        unit: "KM"
      }
    },
    {
      id: "RTPARCAM",
      name: "IBIS PARIS TOUR EIFFEL",
      latitude: 48.84765,
      longitude: 2.30145,
      distance: {
        value: 5.3,
        unit: "KM"
      }
    },
    {
      id: "RDPAR851",
      name: "RADISSON BLU PARIS-BOULOGNE",
      latitude: 48.83648,
      longitude: 2.25321,
      distance: {
        value: 5.31,
        unit: "KM"
      }
    },
    {
      id: "BWPAR456",
      name: "BEST WESTERN HOTEL DE WEHA",
      latitude: 48.83009,
      longitude: 2.35679,
      distance: {
        value: 5.34,
        unit: "KM"
      }
    },
    {
      id: "HSPARAZY",
      name: "SAPHIR GRENELLE",
      latitude: 48.84841,
      longitude: 2.29703,
      distance: {
        value: 5.39,
        unit: "KM"
      }
    },
    {
      id: "HSORYAHF",
      name: "CARINA TOUR EIFFEL",
      latitude: 48.84799,
      longitude: 2.2872,
      distance: {
        value: 5.42,
        unit: "KM"
      }
    },
    {
      id: "ACPARF58",
      name: "MAYET",
      latitude: 48.84724,
      longitude: 2.31836,
      distance: {
        value: 5.42,
        unit: "KM"
      }
    },
    {
      id: "HSJDPAAA",
      name: "ART HOTEL EIFFEL",
      latitude: 48.84876,
      longitude: 2.29847,
      distance: {
        value: 5.42,
        unit: "KM"
      }
    },
    {
      id: "AZPAROEI",
      name: "CITADINES TOUR EIFFEL PARIS",
      latitude: 48.8489,
      longitude: 2.29806,
      distance: {
        value: 5.44,
        unit: "KM"
      }
    }
  ],
  message: "",
  status: 1
}

function formatDataHotel(data){
  var newData = []
  data.map((element, index) => {
    newData.push({
      place_name: element.name,
      latitude: element.latitude,
      longitude: element.longitude,
      buttonText: "Ajouter au trajet"
    })
  })
  return newData
}

export {data, formatDataHotel};

