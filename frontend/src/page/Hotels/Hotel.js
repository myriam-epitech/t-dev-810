import React, { useState, useEffect } from "react";
import SectionResult from "../../component/Results/SectionResults";
import Travel from "../../assets/images/travelers.svg";
import { data, formatDataHotel } from "./data";
import { post } from "../../helpers/axiosHelper";
import { resultType } from "../../assets/constants/type";

function Hotels() {
  const initialLatLng = [48.8, 2.3];
  const [locationData, setLocationData] = useState({});
  const [latLng, setLatLng] = useState([45.757814, 4.835]);

  useEffect(() => {
    if (localStorage.getItem("Place") === null) return;

    const latLng = localStorage.getItem("Place").split(",");

    post("location/hotel", {
      // coordinates: { latitude: latLng[0], longitude: latLng[1] },
      coordinates: { latitude: initialLatLng[0], longitude: initialLatLng[1] },
    }).then((resp) => {
      //I'm using fake data since API does not work properly
      // setLocationData(locationData.data);
    });

    setLocationData(formatDataHotel(data.data));
  }, []);

  return (
    <div className="container">
      <section className="header">
        <img className="background-image" src={Travel} />
        <h1 data-testid="hotel-page">Où veux tu loger ?</h1>
      </section>

      <SectionResult cardType={resultType.HOTEL} locationData={locationData} latLng={latLng} />
    </div>
  );
}

export default Hotels;
