import * as React from 'react';
import MyTravelsList from '../../component/Auth/MyTravelsList';
import Cookie from 'js-cookie'
import { useNavigate } from 'react-router-dom';
import Alert from '../../helpers/notifications';
import { Box, Typography } from '@mui/material';

export default function MyTravels() {
  const navigate = useNavigate();

  React.useEffect(() => {
    if (!Cookie.get("id")) {
      navigate('/')
      Alert('error', 'Connect to access to your travels', '')
    }
  }, [Cookie.get("id")]);

  return (
    <>
    <Box margin={10}>
    <Typography variant="h3" color="text.secondary" align="center" >
        My travels
      </Typography>
      <MyTravelsList/>
    </Box>
    </>
  );
}