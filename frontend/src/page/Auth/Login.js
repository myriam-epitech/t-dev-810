import { useEffect } from 'react';
import LoginForm from '../../component/Auth/LoginForm';
import Cookie from 'js-cookie'
import { useNavigate } from 'react-router-dom';

export default function Login() {
  const navigate = useNavigate();

  useEffect(() => {
    if (Cookie.get("id") !== undefined) {
      navigate('/')
    }
  }, [Cookie]);
  return (
    <LoginForm/>
  );
}