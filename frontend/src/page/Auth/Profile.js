import * as React from 'react';
import ProfileForm from '../../component/Auth/ProfileForm';
import Cookie from 'js-cookie'
import { useNavigate } from 'react-router-dom';

export default function Profile() {
  const navigate = useNavigate();

  React.useEffect(() => {
    if (!Cookie.get("id")) {
      navigate('/')
    }
  }, [Cookie]);

  return (
    <ProfileForm/>
  );
}