import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import axios from "axios";
import Home from "./Home";
import { waitFor } from "@testing-library/react";
import { act } from "react-dom/test-utils";
import { BrowserRouter as Router } from "react-router-dom";


jest.mock("axios");


describe("Home Component", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  test("should update latLng state variable with location data", async () => {
    const mockLocationData = [
      {
        latitude: 37.7749,
        longitude: -122.4194,
      },
    ];
    axios.post.mockResolvedValueOnce({ data: { data: mockLocationData } });
    render(<Router><Home /></Router>);

    const searchInput = screen.getByPlaceholderText("Où veux tu voyager ?");
    const searchButton = screen.getByTestId("searchButtonHome");

    await act(() => {
      userEvent.type(searchInput, "San Francisco");

      userEvent.click(searchButton);

      expect(axios.post).toHaveBeenCalledWith("location", {
        location: "San Francisco",
      });
    });
  });

  test("changeMapLocation method should set latLng state to the provided location", async () => {
    render(<Router><Home /></Router>);
    const location = [40.7128, -74.006];
    const responseData = { data: { location: location } };
    axios.post.mockResolvedValue(responseData);

    const input = screen.getByPlaceholderText("Où veux tu voyager ?");
    fireEvent.change(input, { target: { value: location } });

    const button = screen.getByRole("button");
    fireEvent.click(button);

    await waitFor(() =>
      expect(axios.post).toHaveBeenCalledWith(
        "location",
        {location : "40.7128,-74.006"}
      )
    );

    const response = await axios.post.mock.results[0].value;
    expect(response).toStrictEqual({ data: { location: location } });
  });
});
