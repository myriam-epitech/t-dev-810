import React from "react";
import App from "../../App";
import axios from "axios";

import { render, screen, fireEvent, waitFor } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";

jest.mock('axios');

describe("Home tests", () => {
  test("renders home page", () => {
    render(
      <MemoryRouter initialEntries={["/"]}>
        <App />
      </MemoryRouter>
    );
    const homePage = screen.getByTestId("home-page");
    expect(homePage).toBeInTheDocument();
  });

  test("SHould send axios request properly and should get status 1", async () => {
    const location = "Paris";
    const responseData = { data: { location: location } };
    axios.post.mockResolvedValue(responseData);

    render(
      <MemoryRouter initialEntries={["/"]}>
        <App />
      </MemoryRouter>
    );
    const input = screen.getByPlaceholderText("Où veux tu voyager ?");
    fireEvent.change(input, { target: { value: location } });

    const button = screen.getByTestId("searchButtonHome");
    fireEvent.click(button);

    await waitFor(() =>
      expect(axios.post).toHaveBeenCalledWith(
        "location",
        {location : location}
      )
    );
    const response = await axios.post.mock.results[0].value;
    expect(response).toStrictEqual({ data: { location: location } });
  });
});
