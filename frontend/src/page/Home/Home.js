import React, { useEffect, useState } from "react";

import World from "../../assets/images/world.svg";
import Searchbar from "../../component/Searchbar/Searchbar";
import { post, get } from "../../helpers/axiosHelper";

import "../../assets/scss/home.scss";

import SectionResult from "../../component/Results/SectionResults";
import { resultType } from "../../assets/constants/type";

import { useSearchParams } from "react-router-dom"
import Alert from '../../helpers/notifications';
import { ROUTES } from '../../assets/constants/router';
import { useNavigate } from 'react-router-dom';
import Cookie from 'js-cookie'
import { CircularProgress, Box } from '@mui/material';

function Home() {
  const [locationData, setLocationData] = useState({});
  const [latLng, setLatLng] = useState([45.757814, 4.835]);
  const [loading, setLoading] = useState(true);
  const [queryParameters] = useSearchParams();
  const navigate = useNavigate();

  async function searchLocation(location) {
    const response = await post("location", { location: location });
    setLocationData(response.data.data);
  }

  useEffect(() => {
    if (locationData[0] === undefined) return;
    const newLatLng = [locationData[0].latitude, locationData[0].longitude];
    setLatLng(newLatLng);

    localStorage.setItem("Place", latLng);
  }, [locationData]);


  useEffect(() => {
    if (queryParameters.get('code')) {
      setLoading(true)
      const url = window.location.href
      console.log("dans le use effect1")
      get(url.replace('http://localhost:3000/home', 'google/login')).then((response) => {
        if (response.data.status == 1) {
          Cookie.set('token', response.data.data.token)
          Cookie.set('id', response.data.data.user.id)
          Cookie.set('username', response.data.data.user.username)
          Cookie.set('email', response.data.data.user.email)
          navigate(ROUTES.home)
          setLoading(false)
          Alert('success', response.data.message)
        } else {
          navigate(ROUTES.login)
          Alert('error', "Failed to login with Google", "")
        }
      }).catch((err) => {
        navigate(ROUTES.login)
        Alert('error', "Failed to login with Google", "")
        if (err.response.status == 401) {
          Alert('error', 'Wrong credentials. Please, try again', '')
        }
      });
    } else {
      setLoading(false)
    }
    console.log("fin du useeffect2")

  }, [])

  if (loading) return (
    <Box
      sx={{
        marginTop: 8,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
      }}
    >
        <CircularProgress />
    </Box>
  );
  return (
    <div className="container">
      <section className="header">
        <img className="background-image" src={World} alt="background" />
        <h1 data-testid="home-page">Où souhaitez vous partir ?</h1>

        <Searchbar searchLocation={searchLocation} />
      </section>

      <SectionResult
        cardType={resultType.PLACE}
        locationData={locationData}
        latLng={latLng}
      />
    </div>
  );
}

export default Home;
