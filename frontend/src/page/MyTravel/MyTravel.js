import { useEffect, useState } from 'react';
import LoginForm from '../../component/Auth/LoginForm';
import { useParams } from 'react-router-dom';
import { get } from "../../helpers/axiosHelper";
import Alert from '../../helpers/notifications';
import Pdf from '../../component/Pdf/Pdf';

export default function MyTravel() {
  const { id } = useParams()
  const [data, setData] = useState(null)

  useEffect(() => {
    console.log("id", id)
    get(`travel/${id}`).then((response) => {
      if (response.data.status == 1) {
        setData(response.data.data)
      } else {
        Alert('error', "An error occured", '')
      }
    }).catch((err) => {
      Alert('error', 'An error occured, please, reload the page', '')
    });
  }, []);


  return (
      data && data.can_share ?
      <Pdf travel={data}/> : 
      <>
        This travel plan is private
      </>
    )
}