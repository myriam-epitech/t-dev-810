import React, { useState, useEffect } from "react";
import SectionResult from "../../component/Results/SectionResults";
import Travel from "../../assets/images/travelers.svg";
import { data, formatDataPOIS, formatDataTransports } from "./data";
import { resultType } from "../../assets/constants/type";

function Transport() {
  const [locationData, setLocationData] = useState({});
  const [latLng, setLatLng] = useState([45.757814, 4.835]);

  useEffect(() => {
    if (localStorage.getItem("Place") === null) return;

    const latLng = localStorage.getItem("Place").split(",");

    // post("location/pois", {
    //   // coordinates: { latitude: latLng[0], longitude: latLng[1] },
    //   coordinates: { latitude: initialLatLng[0], longitude: initialLatLng[1] },
    // }).then((resp) => {
    //   //Since the API does'nt work properly, i extracted a json data. The code should be below
    //   // setLocationData(locationData.data);
    //
    // });

    // setLocationData(formatDataTransports(data.data.pois))
  });

  return (
    <div className="container">
      <section className="header">
        <img className="background-image" src={Travel} />
        <h1 data-testid="transports-page">Comment souhaitez vous vous y rendre ?</h1>
      </section>

      <SectionResult cardType={resultType.PLACE} locationData={locationData} latLng={latLng} />
    </div>
  );
}

export default Transport;
