const data = {}

function formatDataTransports(data){
  var newData = []
  data.map((element, index) => {
    newData.push({
      place_name: "element.name",
      latitude: "element.coordinates.latitude",
      longitude: "element.coordinates.longitude",
      buttonText: "Ajouter au trajet"
    })
  })
  return newData
}

export {data, formatDataTransports};

