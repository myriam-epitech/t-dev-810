import React from "react";
import { Route, Routes } from "react-router-dom";

import {NotificationContainer} from 'react-notifications';

import Sidebar from "./component/Sidebar/Sidebar";
import Home from "./page/Home/Home";
import Pois from "./page/Pois/Pois";
import Hotels from "./page/Hotels/Hotel";
import Menu from "./component/Auth/Menu";
import Login from "./page/Auth/Login";
import Register from "./page/Auth/Register";
import 'mapbox-gl/dist/mapbox-gl.css';
import Profile from "./page/Auth/Profile";
import MyTravels from "./page/Auth/MyTravels";
import Transport from "./page/Transports/Transports";
import Resume from "./page/Resume/Resume";
import MyTravel from "./page/MyTravel/MyTravel";
import 'react-notifications/lib/notifications.css';

function App() {
  return (
    <div className="main-container">
      <Sidebar />

      <NotificationContainer/>
      <section className="app">
        <Menu/>
        <Routes>
          <Route exact path="/" element={<Home />} />
          <Route exact path="/home" element={<Home />} />
          <Route exact path="/pois" element={<Pois />} />
          <Route exact path="/hotel" element={<Hotels />} />
          <Route exact path="/transports" element={<Transport />}/>
          <Route exact path="/login" element={<Login />}/>
          <Route exact path="/register" element={<Register/>}/>
          <Route exact path="/profile" element={<Profile/>}/>
          <Route exact path="/my-travels" element={<MyTravels/>}/>
          <Route exact path="/resume" element={<Resume />}/>
          <Route exact path="/my-travel/:id" element={<MyTravel />}/>
        </Routes>
      </section>
    </div>
  );
}

export default App;
