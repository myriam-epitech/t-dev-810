import { NotificationManager} from 'react-notifications';

function Alert(type, message, infos) {
    switch (type) {
        case 'info':
            NotificationManager.info(message);
            break;
        case 'success':
            NotificationManager.success(message, infos);
            break;
        case 'warning':
            NotificationManager.warning(message, infos, 3000);
            break;
        case 'error':
            NotificationManager.error(message, infos, 5000, () => {
                alert('callback');
            });
            break;
        default:
            NotificationManager.success(message, infos);
            break;
        }
}

export default Alert;