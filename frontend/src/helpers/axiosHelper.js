import axios from "axios";
import Cookie from 'js-cookie';

async function post(route, params) {

  const token = Cookie.get("token");
  return axios.post(`http://localhost:8000/api/${route}`, params, {
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Content-Type": "application/json",
      "Authorization": `Bearer ${token}`
    },
  });
}

async function get(route) {
  const token = Cookie.get("token");
  return axios.get(`http://localhost:8000/api/${route}`, {
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Credentials": true,
      "Access-Control-Allow-Headers": "Cookie",
      "Content-Type": "application/json",
      "Authorization": `Bearer ${token}`
    },
  });
}


async function patch(route, params) {
  const token = Cookie.get("token");
  return axios.patch(`http://localhost:8000/api/${route}`, params, {
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Credentials": true,
      "Access-Control-Allow-Headers": "Cookie",
      "Content-Type": "application/json",
      "Authorization": `Bearer ${token}`
    },
  });
}

async function deleteAxios(route) {
  const token = Cookie.get("token");
  return axios.delete(`http://localhost:8000/api/${route}`, {
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Credentials": true,
      "Access-Control-Allow-Headers": "Cookie",
      "Content-Type": "application/json",
      "Authorization": `Bearer ${token}`
    },
  });
}

export { post, get, patch, deleteAxios};
