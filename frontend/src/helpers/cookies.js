import Cookie from 'js-cookie';

export function logout() {
    Cookie.remove('token')
    Cookie.remove('username')
    Cookie.remove('email')
    Cookie.remove('id')
}
