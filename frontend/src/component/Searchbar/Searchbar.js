import React, { useEffect, useState } from "react";
import "../../assets/scss/searchbar.scss";

function Searchbar({ searchLocation }) {
  const [searchText, setSearchText] = useState("");

  function changeSearchText(event) {
    const { value } = event.target;
    setSearchText(value);
  }

  function submitSearch(event) {
    event.preventDefault();

    if (searchLocation) searchLocation(searchText);
  }

  return (
    <div className="search-container">
      <form className="search" onSubmit={submitSearch}>
        <input
          type="text"
          className="searchTerm"
          placeholder="Où veux tu voyager ?"
          onChange={changeSearchText}
        />
        <button
          onClick={submitSearch}
          role="button"
          type="submit"
          className="searchButton"
          data-testid="searchButtonHome"
        >
          <i className="fa fa-search"></i>
        </button>
      </form>
    </div>
  );
}

export default Searchbar;
