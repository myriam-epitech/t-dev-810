import React from "react";
import { screen, render, fireEvent, waitFor } from "@testing-library/react";
import userEvent from "@testing-library/user-event";

import Searchbar from "./Searchbar";

describe("Searchbar tests", () => {
  let searchLocation = jest.fn();

  beforeEach(() => {
    render(<Searchbar searchLocation={searchLocation}/>);
  });

  test("renders without crashing", () => {
    const searchInput = screen.getByPlaceholderText("Où veux tu voyager ?");
    expect(searchInput).toBeInTheDocument();
  });

  test("renders search input field", () => {
    const searchInput = screen.getByPlaceholderText("Où veux tu voyager ?");
    expect(searchInput).toBeInTheDocument();
    expect(searchInput).toHaveAttribute("type", "text");
    expect(searchInput).toHaveAttribute("class", "searchTerm");
  });

  test("renders search button", () => {
    const searchButton = screen.getByRole("button");
    expect(searchButton).toBeInTheDocument();
    expect(searchButton).toHaveAttribute("type", "submit");
    expect(searchButton).toHaveAttribute("class", "searchButton");
  });

  test("calls submitSearch method when button is clicked", () => {
    const searchButton = screen.getByRole("button");
    userEvent.click(searchButton);
  });

  it("updates search text when input value changes",async () => {
    const input = screen.getByPlaceholderText("Où veux tu voyager ?");

    fireEvent.change(input, { target: { value: "Paris" } });

    await waitFor(() => {
      expect(input.value).toBe("Paris");
    })
  });

  it("does not submit the form when button is clicked", () => {
    const button = screen.getByRole("button");

    searchLocation.mockResolvedValueOnce({ data: { location: "paris" } });
    
    fireEvent.click(button);

    expect(searchLocation).toHaveBeenCalled()
    expect(button).toHaveAttribute("type", "submit");
  });
});
