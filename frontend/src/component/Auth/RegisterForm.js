import React, { useState } from 'react';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import Link from '@mui/material/Link';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { post } from "../../helpers/axiosHelper";
import { ROUTES } from '../../assets/constants/router';
import { useNavigate } from 'react-router-dom';
import Cookie from 'js-cookie'
import Alert from '../../helpers/notifications';

function Copyright(props) {
  return (
      <Typography variant="body2" color="text.secondary" align="center" {...props}>
          {'Copyright © '}
          <Link color="inherit" >
              T-WEB-810
          </Link>{' '}
          {new Date().getFullYear()}
          {'.'}
      </Typography>
  );
}

const theme = createTheme();

export default function RegisterForm() {
    const navigate = useNavigate();
    const [data, setData] = useState({
        password: "",
        password_confirmation: "",
        email: "",
        username: "",
    })

    function onInputChange(e) {
    const newdata = { ...data }
    newdata[e.target.id] = e.target.value
    setData(newdata)
    }

  function handleSubmit() {
    post('users', data).then((response) => {
        if(response.data.status == 1) {
            Cookie.set('token', response.data.data)
            navigate(ROUTES.login)

            Alert('success', 'Profile created')
        } else {
            Alert('error', 'Wrong credentials. Please, try again', '')
        }
    }).catch((err) => {
        if(err.response.status == 401 ){
            Alert('error', 'Wrong credentials. Please, try again', '')
        } else {
            Alert('error', 'An error occured, please try later.', '')
        }
    });
}

  return (
    <ThemeProvider theme={theme}>
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Box
          sx={{
            marginTop: 8,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
          <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign in
          </Typography>
          <Box noValidate sx={{ mt: 1 }}>
            <TextField
              data-testid="username"
              margin="normal"
              required
              fullWidth
              id="username"
              label="Username"
              name="username"
              autoComplete="username"
              autoFocus
              onChange={(e) => onInputChange(e)}
            />
            <TextField
              data-testid="email"
              margin="normal"
              required
              fullWidth
              id="email"
              label="Email Address"
              name="email"
              autoComplete="email"
              autoFocus
              onChange={(e) => onInputChange(e)}
            />
            <TextField
              data-testid="password"
              margin="normal"
              required
              fullWidth
              name="password"
              label="Password"
              type="password"
              id="password"
              autoComplete="current-password"
              onChange={(e) => onInputChange(e)}
            />
            <TextField
                data-testid="password_confirmation"
               margin="normal"
               required
               fullWidth
               name="password_confirmation"
               label="Password confirmation"
               type="password"
               id="password_confirmation"
               autoComplete="current-password"
               onChange={(e) => onInputChange(e)}
           />
            <Button
              data-testid="buttonRegisterForm"
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 15 }}
              onClick={handleSubmit}
            >
              Register
            </Button>
          </Box>
        </Box>
        <Copyright sx={{ mt: 15, mb: 0 }} />
      </Container>
    </ThemeProvider>
  );
}