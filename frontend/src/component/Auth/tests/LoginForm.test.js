import React from "react"
import LoginForm from "../LoginForm";
import { BrowserRouter as Router } from "react-router-dom";
import { render, screen, fireEvent, waitFor } from "@testing-library/react";
import axios from "axios";
jest.mock('axios');
describe("Test LoginForm page", () => {
  test("render page", () => {
    render(<Router><LoginForm /></Router>)
  })

  test('renders Loginform page with email field', () => {
    render(<Router><LoginForm /></Router>)
    const field = screen.getByTestId("email")
    expect(field).toHaveTextContent("Email Address");
    expect(field).toBeInTheDocument();
  });

  test('renders LoginForm page with password field', () => {
    render(<Router><LoginForm /></Router>)
    const field = screen.getByTestId("password")
    expect(field).toHaveTextContent("Password");
    expect(field).toBeInTheDocument();
  });

  test('renders LoginForm page with button submit', () => {
    render(<Router><LoginForm /></Router>)
    const field = screen.getByTestId("buttonLoginForm")
    expect(field).toHaveTextContent("Login");
    expect(field).toBeInTheDocument();
  });

  test('renders LoginForm page with button Google submit', () => {
    render(<Router><LoginForm /></Router>)
    const field = screen.getByTestId("googleButtonLoginForm")
    expect(field).toHaveTextContent("Login with Google");
    expect(field).toBeInTheDocument();
  });

  test('submit the form', async () => {
    const email = "test";
    const responseData = { data: { email: email } };
    axios.post.mockResolvedValue(responseData);

    const page = render(<Router><LoginForm /></Router>)
    const input = page.container.querySelector('#email')
    // const input = screen.getByTestId("email")
    fireEvent.change(input, { target: { value: email } });

    const button = screen.getByTestId("buttonLoginForm");
    fireEvent.click(button);

    await waitFor(() =>
      expect(axios.post).toHaveBeenCalledWith(
        "login",
        {email : email, password: ""}
      )
    );
    const response = await axios.post.mock.results[0].value;
    expect(response).toStrictEqual({ data: {email: email} });
  });

})