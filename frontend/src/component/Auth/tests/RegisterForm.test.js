import React from "react"
import RegisterForm from "../RegisterForm";
import { BrowserRouter as Router } from "react-router-dom";
import { render, screen, fireEvent, waitFor } from "@testing-library/react";
import axios from "axios";
jest.mock('axios');
describe("Test RegisterForm page", () => {
  test("render page", () => {
    render(<Router><RegisterForm /></Router>)
  })

  test('renders RegisterForm page with username field', () => {
    render(<Router><RegisterForm /></Router>)
    const field = screen.getByTestId("username")
    expect(field).toHaveTextContent("Username");
    expect(field).toBeInTheDocument();
  });

  test('renders RegisterForm page with email', () => {
    render(<Router><RegisterForm /></Router>)
    const field = screen.getByTestId("email")
    expect(field).toHaveTextContent("Email Address");
    expect(field).toBeInTheDocument();
  });

  test('renders RegisterForm page with password', () => {
    render(<Router><RegisterForm /></Router>)
    const field = screen.getByTestId("password")
    expect(field).toHaveTextContent("Password");
    expect(field).toBeInTheDocument();
  });

  test('renders RegisterForm page with button Password confirmation', () => {
    render(<Router><RegisterForm /></Router>)
    const field = screen.getByTestId("password_confirmation")
    expect(field).toHaveTextContent("Password confirmation");
    expect(field).toBeInTheDocument();
  });

  test('renders RegisterForm page with button Register', () => {
    render(<Router><RegisterForm /></Router>)
    const field = screen.getByTestId("buttonRegisterForm")
    expect(field).toHaveTextContent("Register");
    expect(field).toBeInTheDocument();
  });

  test('submit the form', async () => {
    const email = "test";
    const responseData = { data: { email: email } };
    axios.post.mockResolvedValue(responseData);

    const page = render(<Router><RegisterForm /></Router>)
    const input = page.container.querySelector('#email')
    fireEvent.change(input, { target: { value: email } });

    const button = screen.getByTestId("buttonRegisterForm");
    fireEvent.click(button);

    await waitFor(() =>
      expect(axios.post).toHaveBeenCalledWith(
        "users",
        {
          email : email,
          password: "",
          password_confirmation: "",
          username: "",
        }
      )
    );
    const response = await axios.post.mock.results[0].value;
    expect(response).toStrictEqual({ data: {email: email} });
  });

})