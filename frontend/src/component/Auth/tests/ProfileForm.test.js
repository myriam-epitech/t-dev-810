import React from "react"
import ProfileForm from "../ProfileForm";
import { BrowserRouter as Router } from "react-router-dom";
import { render, screen, fireEvent, waitFor } from "@testing-library/react";
import axios from "axios";

jest.mock('axios');

describe("Test ProfileForm page", () => {
  test("render page", () => {
    render(<Router><ProfileForm /></Router>)
  })

  test('renders ProfileForm page with username field', () => {
    render(<Router><ProfileForm /></Router>)
    const field = screen.getByTestId("username")
    expect(field).toHaveTextContent("Username");
    expect(field).toBeInTheDocument();
  });

  test('renders ProfileForm page with email', () => {
    render(<Router><ProfileForm /></Router>)
    const field = screen.getByTestId("email")
    expect(field).toHaveTextContent("Email Address");
    expect(field).toBeInTheDocument();
  });
  
  test('renders ProfileForm page with button old confirmation', () => {
    render(<Router><ProfileForm /></Router>)
    const field = screen.getByTestId("old_password")
    expect(field).toHaveTextContent("Enter your current password");
    expect(field).toBeInTheDocument();
  });

  test('renders ProfileForm page with password', () => {
    render(<Router><ProfileForm /></Router>)
    const field = screen.getByTestId("password")
    expect(field).toHaveTextContent("Enter your new password");
    expect(field).toBeInTheDocument();
  });

  test('renders ProfileForm page with button Password confirmation', () => {
    render(<Router><ProfileForm /></Router>)
    const field = screen.getByTestId("password_confirmation")
    expect(field).toHaveTextContent("Password confirmation");
    expect(field).toBeInTheDocument();
  });

  test('renders ProfileForm page with button Register', () => {
    render(<Router><ProfileForm /></Router>)
    const field = screen.getByTestId("buttonProfileForm")
    expect(field).toHaveTextContent("Register");
    expect(field).toBeInTheDocument();
  });

  test('submit the form', async () => {
    const email = "test";
    const responseData = { data: { email: email } };
    axios.patch.mockResolvedValue(responseData);

    const page = render(<Router><ProfileForm /></Router>)
    const input = page.container.querySelector('#email')
    fireEvent.change(input, { target: { value: email } });

    const button = screen.getByTestId("buttonProfileForm");
    fireEvent.click(button);

    await waitFor(() =>
      expect(axios.patch).toHaveBeenCalledWith(
        "users/undefined",
        {
          email : email,
          username: undefined,
        }
      )
    );
    const response = await axios.patch.mock.results[0].value;
    expect(response).toStrictEqual({ data: {email: email} });
  });

})