import React from "react"
import AccountPopover from "../AccountPopover";
import { render, screen, fireEvent, getByLabelText, waitFor } from "@testing-library/react";
import { BrowserRouter as Router } from "react-router-dom";

test("It renders some menu items", async () => {
  const rendu = render(<Router><AccountPopover /></Router>);
  const button = rendu.getByTestId("AccountPopoverButton");

  fireEvent.click(button);

  const field = screen.getByTestId("AccountPopoverButtonOpen");


  expect(field).toHaveTextContent("My travels");
  expect(field).toHaveTextContent("Profile");
  expect(field).toHaveTextContent("Logout");
});