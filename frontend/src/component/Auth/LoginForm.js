import React, { useState } from 'react';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Link from '@mui/material/Link';
import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { post } from "../../helpers/axiosHelper";
import { ROUTES } from '../../assets/constants/router';
import { useNavigate } from 'react-router-dom';
import Cookie from 'js-cookie'
import Alert from '../../helpers/notifications';
import { Divider } from '@mui/material';

function Copyright(props) {
    return (
        <Typography variant="body2" color="text.secondary" align="center" {...props}>
            {'Copyright © '}
            <Link color="inherit" >
                T-WEB-810
            </Link>{' '}
            {new Date().getFullYear()}
            {'.'}
        </Typography>
    );
}

const theme = createTheme();

export default function LoginForm() {
    const navigate = useNavigate();
    const [data, setData] = useState({
        password: "",
        email: "",
    })

    function onInputChange(e) {
        const newdata = { ...data }
        newdata[e.target.id] = e.target.value
        setData(newdata)
    }

    function handleSubmit() {
        post('login', data).then((response) => {
            if(response.data.status == 1) {
                Cookie.set('token', response.data.data.token)
                Cookie.set('id', response.data.data.user.id)
                Cookie.set('username', response.data.data.user.username)
                Cookie.set('email', response.data.data.user.email)
                setTimeout(()=>{
                    navigate(ROUTES.home)
                    Alert('success', response.data.message)
                }, 2000)
            } else {
                Alert('error', 'Wrong credentials. Please, try again', '')
            }
        }).catch((err) => {
            if(err.response.status == 401 ){
                Alert('error', 'Wrong credentials. Please, try again', '')
            } else {
                Alert('error', 'An error occured, please try later.', '')
            }
        });
    }

    function redirectToGoogleAuth() {
        window.location.assign("http://localhost:8000/api/redirect")
    }

    return (
        <ThemeProvider theme={theme}>
            <Grid container component="main" sx={{ height: '100vh' }}>
                <CssBaseline />
                <Grid
                    item
                    xs={false}
                    sm={4}
                    md={7}
                    sx={{
                        backgroundImage: 'url(https://source.unsplash.com/random)',
                        backgroundRepeat: 'no-repeat',
                        backgroundColor: (t) =>
                            t.palette.mode === 'light' ? t.palette.grey[50] : t.palette.grey[900],
                        backgroundSize: 'cover',
                        backgroundPosition: 'center',
                    }}
                />
                <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
                    <Box
                        sx={{
                            my: 10,
                            mx: 4,
                            display: 'flex',
                            flexDirection: 'column',
                            alignItems: 'center',
                        }}
                    >
                        <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
                            <LockOutlinedIcon />
                        </Avatar>
                        <Typography component="h1" variant="h5">
                            Sign in
                        </Typography>
                        <Box noValidate sx={{ mt: 1 }}>
                            <TextField
                                data-testid="email"
                                margin="normal"
                                required
                                fullWidth
                                id="email"
                                label="Email Address"
                                name="email"
                                autoComplete="email"
                                autoFocus
                                onChange={(e) => onInputChange(e)}
                            />
                            <TextField
                                data-testid="password"
                                margin="normal"
                                required
                                fullWidth
                                name="password"
                                label="Password"
                                type="password"
                                id="password"
                                autoComplete="current-password"
                                onChange={(e) => onInputChange(e)}
                            />
                            <Button
                                data-testid="buttonLoginForm"
                                fullWidth
                                variant="contained"
                                sx={{ mt: 3, mb: 2 }}
                                onClick={handleSubmit}
                            >
                                Login
                            </Button>
                            <Grid container>
                                <Grid item xs>
                                    {/* <Link href="#" variant="body2">
                                        Forgot password?
                                    </Link> */}
                                </Grid>
                                <Grid item>
                                    <Link href="/register" variant="body2">
                                        {"Don't have an account? Register"}
                                    </Link>
                                </Grid>
                            </Grid>
                                <Divider sx={{ my: 5 }}> OR </Divider>
                        </Box>
                    <Button
                        data-testid="googleButtonLoginForm"
                        variant="outlined"
                        sx={{ my: 12 }}
                        onClick={redirectToGoogleAuth}
                    >
                        <img src='/google-logo.png' width="50"></img>
                        Login with Google
                    </Button>
                    </Box>

                    <Copyright sx={{ mt: 5, alignSelf: 'flex-end' }} />
                </Grid>
            </Grid>
        </ThemeProvider>
    );
}