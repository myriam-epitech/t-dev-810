import * as React from 'react';
import Table from '@mui/material/Table';
import {
  TableBody,
  TableCell,
  Button,
  TableHead,
  TableRow,
  Paper,
  Box,
  Grid,
  capitalize,
  Switch
} from '@mui/material';
import Cookie from 'js-cookie'
import { get, patch, deleteAxios } from "../../helpers/axiosHelper";
import Alert from '../../helpers/notifications';
import { Download, Share, Delete, Visibility, VisibilityOff } from '@mui/icons-material';
import ReactToPdf from "react-to-pdf";
import Pdf from "../Pdf/Pdf";
import { ROUTES } from '../../assets/constants/router';

function createData(name, calories, fat, carbs, protein) {
  return { name, calories, fat, carbs, protein };
}
const rows = [
  createData('Frozen yoghurt', 159, 6.0, 24, 4.0),
  createData('Ice cream sandwich', 237, 9.0, 37, 4.3),
  createData('Eclair', 262, 16.0, 24, 6.0),
  createData('Cupcake', 305, 3.7, 67, 4.3),
  createData('Gingerbread', 356, 16.0, 49, 3.9),
];

export default function MyTravelsList() {
  const options = {
    orientation: 'landscape',
    unit: 'in',
    format: [16,8]
};
  const [selectedTravel, setSelectedTravel] = React.useState(undefined);
  const testRef = React.useRef(null)
  const [data, setData] = React.useState([])

  React.useEffect(() => {
    get(`user/${Cookie.get("id")}/travels`).then((response) => {
      if (response.data.status == 1) {
        setData(response.data.data)
      } else {
        Alert('error', "An error occured", '')
      }
    }).catch((err) => {
      Alert('error', 'An error occured, please, reload the page', '')
    });
  }, [])


  const prepareToDownload = (element) => {
    if (selectedTravel == undefined) {
      setSelectedTravel(element)
    } else {
      setSelectedTravel(undefined)
    }
  }

  const handleChangeCanShare = (event, travel_id, index) => {
    patch(`travel/${travel_id}`, {can_share: event.target.checked}).then((response) => {
      let NewArr = [...data]
      NewArr[index].can_share = !event.target.checked
      if (response.data.status == 1) {
        setData(NewArr)
        if(!event.target.checked){
          Alert('success', "Your travel can now be shared", '')
        } else {
          Alert('success', "Your travel is now private", '')
        }
      } else {
        Alert('error', "An error occured", '')
      }
    }).catch((err) => {
      Alert('error', 'An error occured, please, reload the page', '')
    });
  }
  
  const shareTravel = (travel_id) => {
    navigator.clipboard.writeText(`${ROUTES.host}${ROUTES.my_travel}/${travel_id}`);
    Alert('success', 'Link copied in clipboard')
  }
  
  const deleteTravel = (travel_id, index) => {
    let NewArr = [...data]
    NewArr.splice(index, 1) 
    deleteAxios(`travel/${travel_id}`, {}).then((response) => {
      if (response.data.status == 1) {
          setData(NewArr)
          Alert('success', "Travel deleted successfully", '')
      } else {
        Alert('error', "An error occured", '')
      }
    }).catch((err) => {
      Alert('error', 'An error occured, please, reload the page', '')
    });
  }

  return (
    <Grid item xs={12}>
      <Paper sx={{ p: 1, display: 'flex', flexDirection: 'column' }}>
        <Table sx={{ minWidth: 650 }} size="small">
          <TableHead>
            <TableRow>
              <TableCell align="center">Location</TableCell>
              <TableCell align="center">Can Share</TableCell>
              <TableCell align="center">Created</TableCell>
              <TableCell align="center">Actions</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {data.length == 0 ?
              <TableRow
                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
              >
                You have no travel yet
              </TableRow>
              : data.map((element, index) => (
                <TableRow
                  key={element.id}
                  sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                >
                  <TableCell align="center">{capitalize(element.name)}</TableCell>
                  <TableCell align="center">
                    <Switch
                      checked={element.can_share}
                      onChange={(e) => handleChangeCanShare(e, element.id, index)}
                      inputProps={{ 'aria-label': 'controlled' }}
                    />
                  </TableCell>
                  <TableCell align="center">{(element.created_at).split('T')[0]}</TableCell>
                  <TableCell align="center">

                    <Button onClick={() => prepareToDownload(element)}>
                      {
                        selectedTravel && selectedTravel.id == element.id ?
                          <VisibilityOff sx={{ margin: 1 }} /> :
                          <Visibility sx={{ margin: 1 }} />
                      }
                    </Button>
                    <Button onClick={() => shareTravel(element.id)} disabled={!element.can_share}><Share sx={{ margin: 1 }} /></Button>
                    <Button onClick={() => deleteTravel(element.id, index)}><Delete sx={{ margin: 1 }} /></Button>
                  </TableCell>
                </TableRow>
              ))}
          </TableBody>
        </Table>
      </Paper>
      {
        selectedTravel &&
        <>
          <Box sx={{ display: 'flex', flexDirection: 'row-reverse' }}>
            <ReactToPdf targetRef={testRef} filename={`${selectedTravel.name}-${selectedTravel.id}.pdf`} options={options}  x={1} y={1} scale={0.8}>
              {({toPdf}) => <Button onClick={toPdf}><Download sx={{ margin: 1 }} />Download in PDF</Button>}
            </ReactToPdf>
          </Box>
          <Box>
            <Pdf reference={testRef} travel={selectedTravel} />
          </Box>
        </>
      }
    </Grid>
  );
}