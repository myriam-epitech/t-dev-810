import * as React from 'react';
import { motion } from "framer-motion";
import { Box, Stack } from '@mui/material';
import { useNavigate } from "react-router-dom";
import { ROUTES } from '../../assets/constants/router';
import Cookie from 'js-cookie'
import AccountPopover from './AccountPopover';
import LoginRegisterPopover from './LoginRegisterPopover';

export default function AuthMenu() {
  let navigate = useNavigate();
  const [isConnected, setIsConnected] = React.useState(false);
  const [refresh, setRefresh] = React.useState(false);

  const handleGoToLogin = () => {
    navigate(ROUTES.login);
  };


  React.useEffect(() => {
    if (Cookie.get("id") !== undefined) {
      setIsConnected(true);
    } else {
      setIsConnected(false);
    }
  }, [Cookie, Cookie.get("id"), refresh]);

  return (
    <Box data-testid="menu-testid" sx={{ display: 'flex', flexDirection: 'row-reverse' }}>
      {isConnected ? (
        <Stack
          direction="row"
          alignItems="center"
          spacing={{
            xs: 0.5,
            sm: 1,
          }}
        >
          <motion.div data-testid="menuaccount-testid" whileHover={{ scale: 1.2 }} whileTap={{ scale: 0.8 }}>
            <AccountPopover />
          </motion.div>
        </Stack>
      ) :
        <motion.div whileHover={{ scale: 1.2 }} whileTap={{ scale: 0.8 }}>
          <LoginRegisterPopover handleGoToLogin={handleGoToLogin}/>
        </motion.div>
      }
    </Box>
  );
}