import React, { useState, useEffect } from 'react';
import CssBaseline from '@mui/material/CssBaseline';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import {
  Avatar,
  Button,
  Typography,
  Box,
  Checkbox,
  FormControlLabel,
  TextField
} from '@mui/material';
import Container from '@mui/material/Container';
import { patch } from "../../helpers/axiosHelper";
import { ROUTES } from '../../assets/constants/router';
import { useNavigate } from 'react-router-dom';
import Cookie from 'js-cookie'
import Alert from '../../helpers/notifications';


export default function ProfileForm() {
  const navigate = useNavigate();
  const [data, setData] = useState({
    email: Cookie.get("email") || "",
    username: Cookie.get("username") || "",
  })

  useEffect(() => {
    setData({
      email: Cookie.get("email"),
      username: Cookie.get("username"),
    })

  }, [Cookie.get("id")])

  function onInputChange(e) {
    const newdata = { ...data }
    newdata[e.target.id] = e.target.value
    setData(newdata)
  }

  function handleSubmit() {
    patch('users/' + Cookie.get("id"), data).then((response) => {
      if (response.data.status == 1) {
        Cookie.set('token', response.data.data)
        Cookie.set('username', response.data.username)
        Cookie.set('email', response.data.email)
        Alert('success', 'Profile edited')
      } else {
        navigate(ROUTES.login)
        Alert('error', 'Expired session, please reconnect', '')
      }
      navigate(ROUTES.profile)
    }).catch((err) => {
      if (err.response.status == 401) {
        navigate(ROUTES.login)
        Alert('error', 'Expired session, please reconnect', '')
      } else {
        Alert('error', 'An error occured, please try later.', '')
      }
    });
  }

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <Box
        sx={{
          marginTop: 8,
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
        }}
      >
        <Avatar src='/avatars/avatar_1.jpg'>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Profile
        </Typography>
        <Box noValidate sx={{ mt: 1 }}>
          <TextField
            margin="normal"
            fullWidth
            id="username"
            data-testid="username"
            label="Username"
            name="username"
            autoComplete="username"
            autoFocus
            value={data.username}
            onChange={(e) => onInputChange(e)}
          />
          <TextField
            margin="normal"
            fullWidth
            id="email"
            data-testid="email"
            label="Email Address"
            name="email"
            autoComplete="email"
            autoFocus
            value={data.email}
            onChange={(e) => onInputChange(e)}
          />
          <TextField
            margin="normal"
            fullWidth
            name="old_password"
            label="Enter your current password"
            type="password"
            id="old_password"
            data-testid="old_password"
            autoComplete="current-password"
            onChange={(e) => onInputChange(e)}
          />
          <TextField
            margin="normal"
            fullWidth
            name="password"
            label="Enter your new password"
            type="password"
            id="password"
            data-testid="password"
            autoComplete="current-password"
            onChange={(e) => onInputChange(e)}
          />
          <TextField
            margin="normal"
            required
            fullWidth
            name="password_confirmation"
            label="Password confirmation"
            type="password_confirmation"
            id="password_confirmation"
            data-testid="password_confirmation"
            autoComplete="current-password"
            onChange={(e) => onInputChange(e)}
          />
          <Button
            fullWidth
            data-testid="buttonProfileForm"
            variant="contained"
            sx={{ mt: 3, mb: 2 }}
            onClick={handleSubmit}
          >
            Register
          </Button>
        </Box>
      </Box>
    </Container>
  );
}