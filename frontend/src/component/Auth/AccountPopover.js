import React, { useState } from 'react';
import Cookie from 'js-cookie'
import { alpha } from '@mui/material/styles';
import { Box, Divider, Typography, Stack, MenuItem, Avatar, IconButton, Popover } from '@mui/material';
import { useNavigate } from "react-router-dom";
import { ROUTES } from '../../assets/constants/router';
import { AccountBox, Map } from '@mui/icons-material';
import { logout } from '../../helpers/cookies';
import Alert from '../../helpers/notifications';

export default function AccountPopover() {
  const [refresh, setRefresh] = useState(false);
  const [open, setOpen] = useState(null);

  let navigate = useNavigate();
  const handleOpen = (event) => {
    setOpen(event.currentTarget);
  };

  const handleClose = () => {
    setOpen(null);
  };

  const GoToMyTravels = () => {
    handleClose()
    navigate(ROUTES.my_travels);
  };

  const GoToMyProfile = () => {
    handleClose()
    navigate(ROUTES.profile);
  };

  const handleLogout = () => {
    logout()
    handleClose()
    navigate(ROUTES.home)
    setRefresh(!refresh);
    Alert('success', "Déconnecté avec succès")
  };

  return (
    <>
      <IconButton
        data-testid="AccountPopoverButton"
        onClick={handleOpen}
        sx={{
          p: 2,
          ...(open && {
            '&:before': {
              zIndex: 1,
              content: "''",
              width: '100%',
              height: '100%',
              borderRadius: '50%',
              position: 'absolute',
              bgcolor: (theme) => alpha(theme.palette.grey[900], 0.8),
            },
          }),
        }}
        >
        <Avatar src='/avatars/avatar_1.jpg' alt="photoURL" />
      </IconButton>

      <Popover
        data-testid="AccountPopoverButtonOpen"
        open={Boolean(open)}
        anchorEl={open}
        onClose={handleClose}
        anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
        transformOrigin={{ vertical: 'top', horizontal: 'right' }}
        PaperProps={{
          sx: {
            p: 0,
            mt: 1.5,
            ml: 0.75,
            width: 180,
            '& .MuiMenuItem-root': {
              typography: 'body2',
              borderRadius: 0.75,
            },
          },
        }}
      >
        <Box sx={{ my: 1.5, px: 2.5 }}>
          <Typography variant="subtitle2" noWrap>
            {Cookie.get("username")}
          </Typography>
          <Typography variant="body2" sx={{ color: 'text.secondary' }} noWrap>
            {Cookie.get("email")}
          </Typography>
        </Box>

        <Divider sx={{ borderStyle: 'dashed' }} />

        <Stack sx={{ p: 1 }}>
          <MenuItem onClick={GoToMyTravels} disableRipple>
            <Map />
            My travels
          </MenuItem>
          <Divider sx={{ my: 0.5 }} />
          <MenuItem onClick={GoToMyProfile} disableRipple>
            <AccountBox />
            Profile
          </MenuItem>
        </Stack>

        <Divider sx={{ borderStyle: 'dashed' }} />

        <MenuItem onClick={handleLogout} sx={{ m: 1 }}>
          Logout
        </MenuItem>
      </Popover>
    </>
  );
}
