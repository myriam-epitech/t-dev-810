import React, { useState } from "react";
import Map from "../../component/Map/Map";
import LocationCards from "../../component/LocationCards/LocationCards";

function SectionResult({ locationData, latLng: initialLatLng, cardType }) {
  const [latLng, setLatLng] = useState(initialLatLng);

  function changeMapLocation(location) {
    setLatLng(location);
  }

  return (
    <section className="location-results">
      <LocationCards
        cardType={cardType}
        data={locationData}
        changeMapLocation={changeMapLocation}
      />
      <Map data-testid="map" latLng={latLng} />
    </section>
  );
}

export default SectionResult;
