import React from "react";

function LocationCard({ cardData, changeMapLocation, keyValue, cardType }) {
  function addDataToTripItems() {
    var tripItems = localStorage.getItem("tripItems");

    if (tripItems === null) {
      tripItems = {
        place: [{ name: "Paris", latitude : localStorage.getItem("Place") }],
      };
    } else {
      tripItems = JSON.parse(tripItems);
    }

    if (tripItems[cardType] === undefined) {
      tripItems[cardType] = [cardData];
    }else{
      tripItems[cardType] = [
        ...tripItems[cardType],
        cardData
      ]
    }

    localStorage.setItem("tripItems", JSON.stringify(tripItems));
  }

  return (
    <div
      key={keyValue}
      data-testid="location-card"
      className="result-card"
      onClick={() => changeMapLocation([cardData.latitude, cardData.longitude])}
    >
      <img
        src={`https://maps.googleapis.com/maps/api/place/photo?${cardData.place_name}`}
      />

      <div className="result-card-item-propositions">
        <h3>{cardData.place_name}</h3>

        <i>
          Latitude: {cardData.latitude} | Longitude: {cardData.longitude}
        </i>
      </div>

     
      {cardType !== "resume" && (
        <button onClick={addDataToTripItems}>
          {cardData.buttonText ? cardData.buttonText : "Partir ici"}
        </button>
      )}
    </div>
  );
}

export default LocationCard;
