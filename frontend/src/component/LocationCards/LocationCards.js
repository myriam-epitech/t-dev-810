import React from "react";
import LocationCard from "./LocationCard";

import "../../assets/scss/location_cards.scss";

function LocationCards({ data, changeMapLocation, cardType }) {
  return (
    <section className="result-cards">
      {Array.from(data).map((element, index) => (
        <LocationCard
          cardType={cardType}
          key={`card-${index}`}
          keyValue={index}
          cardData={element}
          changeMapLocation={changeMapLocation}
        />
      ))}
    </section>
  );
}

export default LocationCards;
