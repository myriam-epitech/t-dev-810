import React from "react";
import { render, fireEvent, screen } from "@testing-library/react";
import LocationCards from "./LocationCards";

describe("LocationCards", () => {
  const data = [
    {
      place_name: "New York City",
      latitude: 40.7128,
      longitude: -74.006,
    },
    {
      place_name: "Los Angeles",
      latitude: 34.0522,
      longitude: -118.2437,
    },
  ];

  test("renders location cards with correct data", () => {
    render(<LocationCards data={data} changeMapLocation={() => {}} />);
    const locationCards = screen.getAllByTestId("location-card");
    expect(locationCards).toHaveLength(data.length);

    data.forEach((cardData, index) => {
      const locationCard = locationCards[index];
      const title = locationCard.querySelector("h3");
      expect(title.textContent).toBe(cardData.place_name);

      const latLngDisplay = locationCard.querySelector("i");
      expect(latLngDisplay.textContent).toBe(`Latitude: ${cardData.latitude} | Longitude: ${cardData.longitude}`);
    });
  });

  test("calls changeMapLocation with correct coordinates when card is clicked", () => {
    const mockChangeMapLocation = jest.fn();
    render(
      <LocationCards data={data} changeMapLocation={mockChangeMapLocation} />
    );
    const locationCards = screen.getAllByTestId("location-card");

    fireEvent.click(locationCards[0]);
    expect(mockChangeMapLocation).toHaveBeenCalledWith([
      data[0].latitude,
      data[0].longitude,
    ]);

    fireEvent.click(locationCards[1]);
    expect(mockChangeMapLocation).toHaveBeenCalledWith([
      data[1].latitude,
      data[1].longitude,
    ]);
  });
});
