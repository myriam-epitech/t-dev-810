import React from "react";
import { screen, render, fireEvent } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import mapboxgl from "mapbox-gl";

import Map from "./Map";

describe("Map component", () => {
  test("renders Map component without crashing", () => {
    render(<Map latLng={[0, 1]} />);
  });

  test("sets up Mapbox map correctly", () => {
    const latLng = [37.7749, -122.4194];
    render(<Map latLng={latLng} />);
    const mapContainer = screen.getByTestId("map-container");
    expect(mapContainer).toBeInTheDocument();
    expect(mapboxgl.Map).toHaveBeenCalled();
  });
});
