import React, { useRef, useEffect, useState } from "react";
import "../../assets/scss/map.scss";

import mapboxgl from "mapbox-gl";
mapboxgl.accessToken =
  "pk.eyJ1IjoibGViYXJvbjQyMCIsImEiOiJjbGVtbXFnNzcwNHoxM3lvOG5rbDNzbmVyIn0.k9M-dg81w8B4pH6FzxUNbw";

function Map({ latLng }) {
  const map = useRef(null);
  const mapContainer = useRef(null);
  const [zoom, setZoom] = useState(9);

  useEffect(() => {
    if (map.current) return;
    map.current = new mapboxgl.Map({
      container: mapContainer.current,
      style: "mapbox://styles/mapbox/streets-v12",
      center: {lat: latLng[0], lng: latLng[1]},
      zoom: zoom,
    });
  });

  useEffect(() => {
    if (!map.current) return;

    map.current.flyTo({center: {lat: latLng[0], lng: latLng[1]}, zoom: zoom })
  }, [latLng]);

  return (
    <div className="mapbox">
      <div ref={mapContainer} className="map-container" data-testid='map-container'/>
    </div>
  );
}

export default Map;
