import React, { useEffect } from "react";
import Tourist from "../../assets/images/tourist_map.svg";
import { post } from "../../../helpers/axiosHelper";

function Fly() {
  useEffect(() => {
    if (localStorage.getItem("Place") === null) return;

    const latLng = localStorage.getItem("Place").split(",");
    post("location/pois", {
      coordinates: { latitude: latLng[0], longitude: latLng[1] },
    }).then((resp) => {
      console.log(resp);
    });
  });

  return (
    <div className="container">
      <section className="header">
        <img className="background-image" src={Tourist} />
        <h1 data-testid="home-page">A voir</h1>
      </section>

      <section className="location-results"></section>
    </div>
  );
}

export default Fly;
