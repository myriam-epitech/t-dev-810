import React, { useState } from "react";
import { NavLink } from "react-router-dom";
import { motion, AnimatePresence } from "framer-motion";

import "../../assets/scss/sidebar.scss";
import { links } from "../../assets/constants/sidebarItems";

import { open } from "../../assets/framer/animate/open";
import { showAnimation } from "../../assets/framer/animate/show";

function Sidebar() {
  const [isOpen, setIsOpen] = useState(false);
  const toggle = () => setIsOpen(!isOpen);

  return (
    <motion.aside animate={open(isOpen)}>
      <div className="sidebar-header">
        <AnimatePresence>
          {isOpen && (
            <motion.h1
              variants={showAnimation}
              initial="hidden"
              animate="show"
              exit="hidden"
              data-testid="sidebar-title"
            >
              Travel
            </motion.h1>
          )}
        </AnimatePresence>{" "}
        <div className="bars">
          <span>
            <i onClick={toggle} className={isOpen ? "fa fa-close" : "fa fa-bars"} data-testid="menu-burger"/>
          </span>
        </div>
      </div>

      <motion.div className="sidebar-content" onHoverStart={toggle} onHoverEnd={toggle}>
        {links.map(({ name, to, id, fa }) => (
          <NavLink to={to} className="link" key={id}>
            <div className="icon">
              <i className={fa}></i>
            </div>
            <AnimatePresence>
              {isOpen && (
                <motion.h2
                  variants={showAnimation}
                  initial="hidden"
                  animate="show"
                  exit="hidden"
                  className="link-to"
                  key={id}
                >
                  {name}
                </motion.h2>
              )}
            </AnimatePresence>
          </NavLink>
        ))}
      </motion.div>
    </motion.aside>
  );
}

export default Sidebar;
