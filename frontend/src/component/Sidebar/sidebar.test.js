import React from "react";
import { fireEvent, render, screen } from "@testing-library/react";

import Sidebar from "./Sidebar";
import { links } from "../../assets/constants/sidebarItems";
import { MemoryRouter } from "react-router-dom";

describe("Sidebar", () => {
  test("number of item inside sidebar", () => {
    render(
      <MemoryRouter>
        <Sidebar />
      </MemoryRouter>
    );

    var howManyLinks = document.getElementsByClassName("link").length;
    expect(howManyLinks).toBe(links.length);
  });

  it("verifies if menu burger opens correctly", () => {
    render(
      <MemoryRouter>
        <Sidebar />
      </MemoryRouter>
    );

    expect(screen.queryByTestId("sidebar-title")).toBeNull();

    const element = screen.getByTestId("menu-burger");
    fireEvent.click(element);

    expect(screen.getByTestId("sidebar-title")).toHaveTextContent("Travel");
  });
});
