import * as React from 'react';
import PropTypes from 'prop-types';
import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Grid';
import Card from '@mui/material/Card';
import CardActionArea from '@mui/material/CardActionArea';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';

function Part2(props) {
  const { pois } = props;

  return (
    <Grid item xs={12} md={6}>
      <CardActionArea component="a" href="#">
        <Card sx={{ display: 'flex' }}>
          <CardContent sx={{ flex: 1 }}>
            <Typography component="h2" variant="h5">
              {pois.name}
            </Typography>
            <Typography variant="subtitle1" color="text.secondary">
              {pois.type}
            </Typography>
            {
            pois.address &&
              <Typography variant="subtitle1" paragraph>
                {pois.address}
              </Typography>
            }
            {
              pois.latitude && pois.longitude &&
              <Typography variant="subtitle1" color="primary">
                {pois.latitude},{pois.longitude}
              </Typography>
            }
          </CardContent>
          <CardMedia
            component="img"
            sx={{ width: 160, height: 150, display: { xs: 'none', sm: 'block' } }}
            image={`https://source.unsplash.com/random/1920x1080/?${pois.type},${pois.name.replace(/\s+/g, '')}`}
            alt={pois.name}
          />
        </Card>
      </CardActionArea>
    </Grid>
  );
}

export default Part2;