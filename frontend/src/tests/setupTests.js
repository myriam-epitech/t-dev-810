import "@testing-library/jest-dom";
import "@testing-library/jest-dom/extend-expect";

jest.mock("mapbox-gl", () => ({
  Map: jest.fn(() => ({
    on: jest.fn(),
    remove: jest.fn(),
    addControl: jest.fn(),
    flyTo: jest.fn(),
  })),
}));
